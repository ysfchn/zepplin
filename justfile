#
#   Copyright (C) 2024 ysfchn
#
#   This file is part of Zepplin.
#
#   Zepplin is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Zepplin is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

JUST := just_executable()

export ZEPPLIN_WEB_PROJECT_FOLDER := justfile_directory() / "zepplin_web"
export ZEPPLIN_WEBVIEW_TARGET_FOLDER := justfile_directory() / "zepplin" / "webview" / "assets" / "vendor"


[private]
@default:
    "{{ JUST }}" --list


# Build zepplin_web
web flavor="zepplin":
    OUTPUT_PATH="{{ ZEPPLIN_WEBVIEW_TARGET_FOLDER }}" "{{ JUST }}" -f "{{ ZEPPLIN_WEB_PROJECT_FOLDER / 'justfile' }}" build "{{ flavor }}"
    cp "{{ ZEPPLIN_WEB_PROJECT_FOLDER / 'node_modules/picnic/picnic.min.css' }}" "{{ ZEPPLIN_WEBVIEW_TARGET_FOLDER / 'picnic.css' }}"