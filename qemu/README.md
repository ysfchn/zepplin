# `qemu`

Zepp Health provides Zepp OS kernel images to make it possible to debug Zepp OS mini-apps in QEMU, a machine emulator & virtualizer software.

Zepp OS runs on ARM MPS2 AN521 boards, however we can't emulate it directly under system provided (such as from `apt`) QEMU binaries, because QEMU has a hard limit on RAM size that MPS2 AN521 boards could have, which is set to 16 MB, but Zepp OS kernel itself requires allocating more resources, so **emulating Zepp OS is not possible without building QEMU upon a custom fork.**

So that's why Zepp Health themselves forked QEMU and made changes on the source code to make emulating Zepp OS possible with QEMU. Their fork can be found at [zepp-health/qemu repository](https://github.com/zepp-health/qemu), and it is based on this [commit `de3f5223fa4cf8bfc5e3fe1fd495ddf468edcdf7`](https://gitlab.com/qemu-project/qemu/-/tree/de3f5223fa4cf8bfc5e3fe1fd495ddf468edcdf7). 

Now we can clone the QEMU and compare the changes against Zepp's fork to create a patch file. Then, the patch can be applied back to the QEMU source code and can be built with these changes.

> Note that while Zepp Health already shared `.patch` files in their GitHub repository, I couldn't manage to emulate Zepp OS on QEMU with these patches, because apparently there was additional changes that have been made in their QEMU fork but not added to these patch files, so we use the patch file that just have been generated.

## Usage

This folder contains a [Justfile](./justfile) that does the all job for us. Install [`just`](https://just.systems/man/en/) on your system, and `cd` to this folder.

### Cloning QEMU and creating patch file

```bash
# All paths are relative to current working directory,
# using absolute paths in parameters may break things!

# 1) Clone Zepp's QEMU fork to a folder named "a"
just clone_zepp "a"

# 2) Clone QEMU to a folder named "b"
just clone_qemu "b"

# 3) Create a patch file with changes against folder "b" and "a",
# and dump the changes in the "zepp_qemu.patch" file.
just create_patch "b" "a" "zepp_qemu.patch"

# 4) Apply the generated patch onto QEMU.
just apply_patch "zepp_qemu.patch" "b"
```

### Building QEMU

Before building QEMU, make sure to install required dependencies to your environment:

```bash
sudo apt update
sudo apt install -y \
    build-essential sparse ninja-build libglib2.0-dev libfdt-dev libpixman-1-dev \
    zlib1g-dev libaio-dev libcurl4-gnutls-dev libgtk-3-dev \
    libibverbs-dev libjpeg8-dev libnuma-dev librdmacm-dev nettle-dev \
    libsasl2-dev libsdl2-dev libvte-2.91-dev libxen-dev xfslibs-dev \
    libcairo2-dev libpango1.0-dev libgif-dev libvirglrenderer-dev libspice-server-dev \
    libusbredirhost-dev libcacard-dev
```

While compiling QEMU, it includes all features that its required dependency is installed on the system. For example, if `libgtk-3-dev` is installed on the system, then the compiled QEMU will have GTK support by default. While this may sound good at first, it means the compiled binary will vary depending on the currently installed system libraries.

So to prevent that, this Justfile tells QEMU to disable all default features and explictly giving the compile flags to make a consistent build for CI. And since we will only be running a Zepp OS image, we don't need to have all the features QEMU offers, so we can only include the flags that we would like.

Flags can be overriden with `QEMU_BUILD_FLAGS` environment variable if you would like.

To build QEMU:

```bash
# 5) Build QEMU and create an tar.gz file named "qemu.tgz" 
# containing the build QEMU binary.
just build_qemu "b" "qemu.tgz"
```

### Do everything at once

There is also a `all` task that runs all tasks described above, which might be helpful if running under a CI environment.

```bash
just all
```