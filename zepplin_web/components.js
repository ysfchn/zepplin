/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * @typedef {import('./controller.js').Controller} Controller
 */

import { createElement } from "preact";

/**
 * @callback ComponentBuilderRenderFunction
 * @returns {void}
 */

/**
 * Component builder.
 * 
 * @typedef {Object} ComponentBuilder
 * @property {ComponentBuilderRenderFunction} render Render this component in a HTML element.
 */

/**
 * A "render"able component.
 * 
 * @param {ComponentBuilderRenderFunction} callable 
 * @returns {ComponentBuilder}
 */
const RenderableComponent = function(callable) {
    return {
        render: callable
    }
}

/**
 * @param {ComponentBuilder | ComponentBuilder[]} renderFuncArr
 */
function makeChildren(renderFuncArr) {
    const children = [];
    if (renderFuncArr !== undefined) {
        if (Array.isArray(renderFuncArr)) {
            for (const elem of renderFuncArr) {
                if ((typeof elem === 'object') && (elem !== null)) {
                    children.push(elem.render());
                } else {
                    children.push(elem);
                }
            }
        } else if ((typeof renderFuncArr === 'object') && (renderFuncArr !== null)) {
            children.push(renderFuncArr.render());
        } else {
            children.push(renderFuncArr);
        }
    }
    return children;
}

/**
 * Settings UI Components.
 */
export class Components {
    /**
     * @param {Controller} controller 
     */
    constructor(controller) {
        this.controller = controller;
    }

    get symbols() {
        return {
            Auth: this.Auth,
            Button: this.Button,
            Image: this.Image,
            Link: this.Link,
            Modal: this.Modal,
            Section: this.Section,
            Slider: this.Slider,
            Text: this.Text,
            TextImageRow: this.TextImageRow,
            TextInput: this.TextInput,
            Toast: this.Toast,
            Toggle: this.Toggle,
            View: this.View
        }
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/auth/
     * TODO: Auth component is not implemented yet.
     * 
     * @param {Object} props 
     * @returns {ComponentBuilder}
     */
    Auth(props) {
        return RenderableComponent(() => {
            console.log(props);
            throw new Error("Auth component is not implemented");
        })
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/button/
     * 
     * @param {Object} props 
     * @param {string} [props.label] Content on the button.
     * @param {Object} [props.style] CSS style properties.
     * @param {string} [props.color] Set component color, one of "default", "primary", "secondary". Default is "default".
     * @param {Function} [props.onClick]
     * @returns {ComponentBuilder}
     */
    Button(props) {
        const {
            color: color = "default",
            label: label,
            style: style,
            onClick: onClick
        } = props;
        return RenderableComponent(() => {
            return createElement("button", {
                class: 
                    color == "default" ? "" : 
                    color == "primary" ? "success" :
                    color == "secondary" ? "warning" :
                    "",
                style: {
                    textTransform: "unset",
                    ...style
                },
                onClick: () => {
                    onClick && onClick();
                }
            }, label);
        })
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/image/
     * 
     * @param {Object} props
     * @param {string} props.src Image URL or base64 string.
     * @param {Object} [props.style] CSS style properties.
     * @param {string} [props.alt] Alternative text when the image cannot be displayed.
     * @param {string | number} [props.width] The width of the component.
     * @param {string | number} [props.height] The height of the component.
     * @returns {ComponentBuilder}
     */
    Image(props) {
        const {
            src: src,
            alt: alt = "",
            width: width,
            height: height,
            style: style
        } = props;
        return RenderableComponent(() => {
            return createElement("img", {
                src: src,
                alt: alt,
                width: width,
                height: height,
                style: style
            });
        })
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/link/
     * 
     * @param {Object} props
     * @param {string} [props.source] Link target.
     * @param {string} textContent Children of this component.
     * @returns {ComponentBuilder}
     */
    Link(props, textContent) {
        const {
            source: source
        } = props;
        return RenderableComponent(() => {
            return createElement("a", {
                href: source,
                target: "_blank"
            }, textContent);
        })
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/modal/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {string} [props.mask] If true (default), mask is rendered.
     * @param {string} [props.maskClosable] If true (default), clicking on the background will trigger onClose.
     * @param {boolean} [props.visible] If true, then modal is visible.
     * @param {Function} [props.onClose] Triggers a callback when the component requests to be closed. Optionally, you can use the reason parameter to control the response to onClose.
     * @param {ComponentBuilder[] | ComponentBuilder} renderFuncArr A renderable or list of renderables as a children.
     * @returns {ComponentBuilder}
     */
    Modal(props, renderFuncArr) {
        const {
            mask: mask = true,
            maskClosable: maskClosable = true,
            visible: visible = true,
            onClose: onClose
        } = props;
        return RenderableComponent(() => {
            console.log(props);
            throw new Error("Modal component is not implemented");
        })
    }    
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/section/
     * 
     * @param {Object} props
     * @param {string} [props.title] Title of this section.
     * @param {string} [props.description] Description of this section.
     * @param {Object} [props.style] CSS style properties.
     * @param {ComponentBuilder[] | ComponentBuilder} renderFuncArr A renderable or list of renderables as a children.
     * @returns {ComponentBuilder}
     */
    Section(props, renderFuncArr) {
        const {
            title: title,
            description: description,
            style: style
        } = props;
        return RenderableComponent(() => {
            return createElement("div", {
                style: style
            },
                title && createElement("div", null, title),
                makeChildren(renderFuncArr),
                description && createElement("div", null, description)
            );
        })
    }

    /**
     * @callback ValueStringChangedCallback
     * @param {string | string[]} value
     * @returns {void}
     */

    /**
     * @typedef SelectOption
     * @property {string} name
     * @property {string} value
     */
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/select/
     * TODO: Implement all properties
     * 
     * @param {Object} props 
     * @param {string} [props.label] Label of this select.
     * @param {SelectOption[]} [props.options] Options of this select.
     * @param {boolean} [props.multiple] If multiple options are allowed, default is false.
     * @param {string | string[]} [props.value] Selected option's value, if multiple options are allowed, it can be a list of strings.
     * @param {ValueStringChangedCallback} [props.onChange] A callback will be fired when the selected option has changed.
     * @param {string} [props.title] Title of this select.
     * @param {string} [props.settingsKey] The key for storing values in the Settings Storage API.
     * @returns {ComponentBuilder}
     */
    Select(props) {
        const {
            label: label,
            options: options,
            multiple: multiple = false,
            title: title,
            onChange: onChange,
            settingsKey: settingsKey,
            value: value
        } = props;
        return RenderableComponent(() => {
            const children = [];
            var selectedValues = value || [];
            if (!Array.isArray(selectedValues)) {
                selectedValues = [selectedValues];
            }
            for (const opt of (options || [])) {
                children.push(createElement("option", {
                    value: opt.value,
                    selected: selectedValues.includes(opt.value)
                }, opt.name));
            }
            return createElement("select", {
                multiple: multiple,
                settingsKey: settingsKey,
                title: title,
                onChange: (ev) => {
                    // @ts-ignore
                    if (!ev.target.multiple) {
                        // @ts-ignore
                        onChange && onChange(ev.target.value);
                    } else {
                        var values = [];
                        // @ts-ignore
                        for (const elem of ev.target.selectedOptions) {
                            values.push(elem.value);
                        }
                        onChange && onChange(values);
                    }
                }
            }, children);
        })
    }
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/slider/
     * 
     * @param {Object} props
     * @param {string} [props.label]
     * @param {number} [props.max]
     * @param {number} [props.min]
     * @param {number} [props.step]
     * @param {number} [props.value]
     * @param {ValueStringChangedCallback} [props.onChange] A callback will be fired when the value has changed.
     * @param {string} [props.settingsKey] The key for storing values in the Settings Storage API.
     * @returns {ComponentBuilder}
     */
    Slider(props) {
        const {
            label: label,
            max: max = 100,
            min: min = 0,
            step: step = 1,
            onChange: onChange,
            settingsKey: settingsKey,
            value: value = 0
        } = props;
        return RenderableComponent(() => {
            return createElement("input", {
                type: "range",
                title: label,
                value: Number.parseFloat(value.toString()),
                max: max,
                min: min,
                step : step,
                settingsKey: settingsKey,
                onChange: (e) => {
                    // @ts-ignore
                    onChange && onChange(e.target.value.toString());
                }
            });
        })
    }
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/text/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {"left" | "center" | "right"} [props.align] Horizontal alignment, support "left", "center", "right". Default is "left".
     * @param {boolean} [props.bold]
     * @param {boolean} [props.italic]
     * @param {boolean} [props.paragraph]
     * @param {Object} [props.style] CSS style properties.
     * @param {string} textContent 
     * @returns {ComponentBuilder}
     */
    Text(props, textContent) {
        const {
            align: align,
            bold: bold = "left",
            italic: italic = false,
            paragraph: paragraph = false,
            style: style
        } = props;
        return RenderableComponent(() => {
            return createElement("p", {
                style: style,
            }, textContent);
        })
    }

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/textimagerow/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {string} [props.label]
     * @param {string} [props.sublabel]
     * @param {string} [props.icon]
     * @param {string} [props.iconColor]
     * @param {boolean} [props.rounded]
     * @param {boolean} [props.iconRight]
     * @returns {ComponentBuilder}
     */
    TextImageRow(props) {
        const {
            label: label,
            sublabel: sublabel,
            icon: icon,
            iconColor: iconColor,
            rounded: rounded = false,
            iconRight: iconRight
        } = props;
        return RenderableComponent(() => {
            return createElement("div", {
                label: label
            });
        })
    }
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/textinput/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {string} [props.label]
     * @param {boolean} [props.disabled]
     * @param {boolean} [props.multiline]
     * @param {string} [props.placeholder]
     * @param {number} [props.rows]
     * @param {boolean} [props.bold]
     * @param {string} [props.value]
     * @param {ValueStringChangedCallback} [props.onChange]
     * @param {string} [props.settingsKey]
     * @param {Object} [props.labelStyle] CSS style properties for text.
     * @param {Object} [props.subStyle] CSS style properties for bottom text.
     * @param {string} [props.type] Type of the input, default is "text".
     * @returns {ComponentBuilder}
     */
    TextInput(props) {
        const {
            label: label,
            disabled: disabled = false,
            multiline: multiline = false,
            placeholder: placeholder = "",
            rows: rows = 1,
            bold: bold = false,
            value: value = "",
            onChange: onChange,
            settingsKey: settingsKey,
            labelStyle: labelStyle,
            subStyle: subStyle,
            type: type = "text"
        } = props;
        return RenderableComponent(() => {
            // @ts-ignore
            return createElement("input", {
                type: type,
                disabled: disabled,
                multiline: multiline,
                placeholder: placeholder,
                value: value,
                onChange: (e) => {
                    // @ts-ignore
                    onChange && onChange(e.target.value);
                },
                rows: rows,
                settingsKey: settingsKey
            });
        })
    }
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/toast/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {number} [props.duration] Duration of the popup prompt, default is 2000.
     * @param {string} [props.horizontal] Horizontal position of toast, default is "center".
     * @param {string} [props.message] Message to display.
     * @param {string} [props.vertical] Vertical position of toast, default is "top".
     * @param {boolean} [props.visible] If true, the Toast is visible.
     * @param {Function} [props.onClose] Triggers a callback when the component requests to be closed.
     * @returns {ComponentBuilder}
     */
    Toast(props) {
        const {
            duration: duration = 2000,
            horizontal: horizontal = "center",
            message: message,
            vertical: vertical = "top",
            visible: visible = true,
            onClose: onClose
        } = props;
        return RenderableComponent(() => {
            // @ts-ignore
            return createElement("div", {
                visible: visible
            }, message);
        })
    }

    /**
     * @callback ValueBooleanChangedCallback
     * @param {boolean} value
     * @returns {void}
     */

    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/toggle/
     * TODO: Implement all properties
     * 
     * @param {Object} props
     * @param {string} [props.label]
     * @param {string} [props.settingsKey]
     * @param {boolean} [props.value]
     * @param {ValueBooleanChangedCallback} [props.onChange]
     * @returns {ComponentBuilder}
     */
    Toggle(props) {
        const {
            label: label,
            settingsKey: settingsKey,
            value: value = false,
            onChange: onChange
        } = props;
        return RenderableComponent(() => {
            return createElement("input", {
                type: "checkbox",
                checked: !!value,
                settingsKey: settingsKey,
                title: label,
                onChange: (e) => {
                    // @ts-ignore
                    onChange && onChange(e.target.value);
                }
            });
        })
    }
    
    /**
     * https://docs.zepp.com/docs/1.0/reference/app-settings-api/ui/view/
     * 
     * @param {Object} props
     * @param {Function} [props.onClick] Triggers a callback when the component requests to be closed.
     * @param {Object} [props.style] CSS style properties.
     * @param {ComponentBuilder[] | ComponentBuilder} renderFuncArr A renderable or list of renderables as a children.
     * @returns {ComponentBuilder}
     */
    View(props, renderFuncArr) {
        const {
            onClick: onClick = () => {},
            style: style
        } = props;
        return RenderableComponent(() => {
            // @ts-ignore
            return createElement("div", {
                style: style,
                onClick: onClick,
            }, makeChildren(renderFuncArr));
        })
    }
}