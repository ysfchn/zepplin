/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

import { Components } from "./components.js";
import { getPeer } from "flavors";
import { 
    Lang, 
    EventBus, 
    getTextFactory, 
    initProxy, 
    formatText,
    Logger,
    createRunnerFunction,
    SettingsStorage,
    bufferToHex
} from "./functions.js";
import { Buffer } from "buffer/index.js";
import { render } from "preact";
import { createServiceAPI } from "./service.js";

export { Buffer };

export class Controller {
    /** @type {Object} */
    languageTable;
    /** @type {Object} */
    userData;
    /** @type {EventBus} */
    events;
    /** @type {SettingsStorage | null} */
    settings;
    /** @type {Logger} */
    logger;

    constructor() {
        this.peer = getPeer();
        this.languageTable = {};
        this.userData = {};
        this.events = new EventBus();
        this.settings = null;
        this.logger = Logger.getLogger("root");
        this.components = new Components(this);
        this._isReady = false;
    }

    /**
     * @returns {string}
     */
    get language() {
        return this.userData["lang"];
    }

    /**
     * @param {string} script 
     */
    async executeScript(script) {
        const module = await this.getModule();
        /** @type {VoidFunction} */
        const runner = createRunnerFunction.call(window, script, {
            ...module,
            // Also, wrap all function and classes in DeviceRuntimeCore, 
            // as this is necessary to achieve compatibility for both 
            // setting and side-app code.
            "DeviceRuntimeCore": module
        });
        runner();
    }

    async getServiceAPI() {
        const peerCaller = await this.peer;
        return createServiceAPI(peerCaller);
    }

    async getModule() {
        const services = await this.getServiceAPI();
        return {
            AppSettingsPage: this.AppSettingsPage(),
            AppSideService: this.AppSideService(),
            console: {
                log: (...args) => this.logger.log(...args),
                debug: (...args) => this.logger.debug(...args),
                info: (...args) => this.logger.info(...args),
                warn: (...args) => this.logger.warn(...args),
                error: (...args) => this.logger.error(...args)
            },
            Buffer: Buffer,
            userSettings: this.userData,
            settings: {
                settingsStorage: this.settings
            },
            messaging: {
                peerSocket: {
                    // Apparently, event name doesn't matter.
                    addListener: (ev, callback) => {
                        this.events.on("bluetooth", (data) => {
                            const d = Buffer.from(data, "hex");
                            callback(d);
                        });
                    },
                    send: (data) => {
                        this.peer.then((peer) => {
                            peer("send_bluetooth", {
                                data: bufferToHex(data)
                            })
                        });
                    }
                }
            },
            HmLogger: this.logger,
            hmSetting: {
                setScreenAutoBright: (value) => 0,
                getScreenAutoBright: () => false,
                getBrightness: () => 0,
                setBrightness: (value) => 0,
                setBrightScreen: (value) => 0,
                setBrightScreenCancel: () => 0,
                setScreenOff: () => 0,
                getUserData: () => ({
                    age: 0,
                    weight: 0,
                    height: 0,
                    gender: 2,
                    nickName: "",
                    region: ""
                }),
                getMileageUnit: () => 0,
                getLanguage: () => this.languageTable[this.language],
                getDateFormat: () => 0,
                getTimeFormat: () => 0,
                getDiskInfo: () => ({
                    total: 0,
                    free: 0,
                    app: 0,
                    watchface: 0,
                    system: 0,
                    music: 0
                }),
                getDeviceInfo: () => ({
                    width: 0,
                    height: 0,
                    screenShape: 0,
                    deviceName: "fakeDevice",
                    keyNumber: 0,
                    deviceSource: 0
                }),
                getWeightTarget: () => 0,
                getWeightUnit: () => 0,
                getScreenType: () => 0,
                getSleepTarget: () => 0
            },
            HmUtils: {
                Lang: Lang,
                EventBus: EventBus,
                Logger: Logger,
                format: (text, ...parameters) => {
                    return formatText(false, text, ...parameters);
                },
                formatStrict: (text, ...parameters) => {
                    return formatText(true, text, ...parameters);
                },
                getDeviceLanguage: () => this.language,
                getLanguage: (userSettings) => { 
                    return userSettings ? userSettings.lang : this.language;
                },
                getSideLanguage: (userSettings) => {
                    return userSettings.lang;
                },
                gettextFactory: getTextFactory,
                initProxy: initProxy,
                printf: (message, ...args) => { this.logger.log(message, ...args) },
                sprintf: (message, ...args) => {
                    return formatText(false, message, ...args);
                }
            },
            ...services,
            ...this.components.symbols
        }
    }

    /**
     * @typedef AppSettingsPageBuilt
     * @property {() => any} render
     */

    /**
     * @callback AppSettingsPageBodyBuild
     * @param {Object} body
     * @param {SettingsStorage} body.settingsStorage
     * @returns {AppSettingsPageBuilt}
     */

    /**
     * @callback AppSettingsPageBody
     * @param {Object} page
     * @param {AppSettingsPageBodyBuild} page.build
     */

    /**
     * @returns {AppSettingsPageBody} 
     */
    AppSettingsPage() {
        return (page) => {
            var rootDiv = document.getElementById("app");
            let builtPage = page.build({
                "settingsStorage": this.settings
            });
            render(builtPage.render(), rootDiv);
            // Re-build UI on settings change.
            this.events.on("refresh", () => {
                let newPage = page.build({
                    "settingsStorage": this.settings
                });
                render(newPage.render(), rootDiv);
            })
        }
    }

    AppSideService() {
        return (page) => {
            page.onInit();
            page.onRun();
        }
    }

    init() {
        return new Promise((resolve, reject) => {
            this.peer.then((peer) => {
                peer("init").then((value) => {
                    if (value.status) {
                        this._isReady = true;
                        resolve();
                    } else {
                        reject("Failed to initialize the peer!");
                    }
                });
            })
        })
    }

    start() {
        if (!this._isReady) {
            throw new Error("Peer is not initialized yet!");
        }
        this.peer.then((peer) => {
            this.logger.on("log", (data) => {
                peer("send_log", data);
            });
            this.events.on("refresh", () => {
                peer("save_data", {
                    app: this.settings.toObject()
                });
            });
            peer("load_data").then((data) => {
                this.languageTable = data.languages;
                this.userData = data.user;
                this.settings = new SettingsStorage(this.events, data.app);
                peer("get_script").then((data) => {
                    Promise.all([
                        data.side ? this.executeScript(data.side) : Promise.resolve(),
                        data.settings ? this.executeScript(data.settings) : Promise.resolve()
                    ]);
                });
            });
        });
    }
}