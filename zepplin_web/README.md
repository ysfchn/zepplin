# `zepplin_web`

Re-implementation of [Side Service and Settings UI APIs for mini-apps.](https://docs.zepp.com/docs/guides/architecture/arc/)

> The current implementation is tested on Zepp OS 1.0. While all mini-apps uses the same side-service app in Zepp app regardless of their target Zepp OS versions, existing interface should work for Zepp OS 2.0 and up for mini-apps if they doesn't use a newly introduced API from these versions.

## Build

This project requires Node.js and `npm` (comes with Node).

This folder contains a [Justfile](./justfile) that contains commands to build the project with eslint. To use it, install [`just`](https://just.systems/man/en/) and run this command:

```bash
just build
```

This command will automatically call `just deps` task to install the required dependencies.

The `build` task takes an argument that defines the which flavor to use. The default is `zepplin`, which builds the project against to use with [Zepplin webview](../zepplin/webview/).

To add a new flavor, add a new entry in `build_config.json` file and add its related code in [`flavors`](./flavors/).

## Original implementation

When a settings page for an mini-app is launched, Zepp opens `http://zepp-os.zepp.com/app-settings/v1.0.0/index.html` (or staging: `http://zepp-os-staging.zepp.com/app-settings/v1.0.0/index.html`) URL in the in-app webview:

Obviously it doesn't display anything as it needs to be provided with necessary communication channel with webview inside the Zepp app, so this webpage it can request the mini-app JavaScript code over it and `eval` it. 

The page loads a single minified JavaScript file, which bundles the other common JavaScript libraries, such as React, Material UI and [many others.](https://bundlescanner.com/bundle/zepp-os.zepp.com%2Fapp-settings%2Fv1.0.0%2Fapp-settings.global.1681974177217.prod.js)

Aside from app settings page, there is also an side-service webpage which manages connection between Zepp app and watch app. Side service webpage powers these services:

* Fetch API
    * Allows to watch apps to send HTTP requests through Zepp app and send back to the watch with `fetch()`, since watches doesn't have network chips, it uses the connected phone as a relay.
* Messaging API
    * Allows sending arbitrary custom binary data over Bluetooth.
* Settings Storage API
    * Allows accessing and managing the preferences for a watch app, which is also exposed app settings page.

Side service page can be accessed in `http://zepp-os.zepp.com/frameworks/v3.0.2/side-service.html`. The page loads a single minified JavaScript file like app settings page, but it is more smaller, since side services don't render any UI, it doesn't require bundling UI frameworks.

## Zepplin implementation

As stated above, Zepp's setting page loads a lot of dependencies, and not everything inside of them are really much used by the setting page. For example, Zepp provides APIs to developers to [render UI components](https://docs.zepp.com/docs/reference/app-settings-api/ui/button/) in setting pages, however, when compared to [Material UI components](https://v4.mui.com/components/buttons/), Zepp only uses very few of them, and in fact, Material UI just used here to provide an default appearance to the components since developers no have access for the APIs other than Zepp's.

Therefore, to prevent the unnecessary bloat, Zepplin's settings page implementation only has these dependencies:

* [Buffer](https://github.com/feross/buffer) - Zepp exposes `Buffer` to the mini-apps, so apps can call `Buffer` API directly without needing to add as dependency to their apps, so this is required in all cases.
* [Preact](https://preactjs.com/) - Used to render settings UI & update it without re-constructing the whole DOM.

Additionally, to make HTML elements to have a default appearance, [Picnic CSS](https://picnicss.com/) is used by Zepplin.

Since Zepplin uses PicnicCSS instead of Material UI as in Zepp, some styling differences between Zepp app and Zepplin's webview is expected. However, even if the difference is just the appearance of the elements, it might still cause incompatibilities if some mini-apps use some custom CSS (because they can inject additional CSS), that may cause settings page having functional issues (e.g. elements are being set to back or front with `z-index` or etc.), but for most apps, it shouldn't affect the usability.

So [this CSS file](../zepplin/webview/assets/fixes.css) serves as a purpose to fix the functionality issues raised on apps that use custom CSS that specifically designed for Material UI.