/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * @typedef {"init" | "send_bluetooth" | "send_log" | "load_data" | "save_data" | "get_script" | "send_http"} CommandType
 * 
 * Type of the command that the peer can receive and send.
 */

/**
 * @callback getPeer
 * @returns {Promise<(command : CommandType, data : object | null) => Promise<object>>}
 * 
 * Returns a callback that when called, passes the data to the peer (e.g. the app that controlling the webview).
 */