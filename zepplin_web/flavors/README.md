Each flavors corresponds a different backend, thus it varies to how to access the object that enabling the communication between the webview and the host that displays the webview.

Zepplin uses `pywebview` module to display a webview from Python. Therefore, pywebview exposes its object named `pywebview` in the Javascript, so there is a tiny wrapper that redirects all bridge commands to it.

Separating these bridge calls to a sub-package allows us to use the same code for different backends.

---

To add a new flavor (backend), simply create an file and export a `getPeer` function which returns a Promise that resolves to a function that accepts the command type and command data.

After adding it, make sure to also add its entry in `exports` in `package.json`, so we can conditionally import only the specified flavor during build step.