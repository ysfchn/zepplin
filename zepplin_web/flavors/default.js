/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/*
    This module is supposed to be replaced during build step by esbuild with one 
    of the flavors defined in this folder. If it is forgetten to do so, then this 
    file serves as an default behavior to halt the execution.
*/

/** 
 * @import * as Types from "./_type.js"
 */

/**
 * @type {Types.getPeer}
 */
export function getPeer() {
    return new Promise((resolve, reject) => {
        document.body.innerHTML = "";
        const p = document.createElement("p");
        p.innerText = "No peer is available. Build zepplin_web with a valid flavor!";
        document.body.appendChild(p);
        reject(new Error(p.innerText));
    });
};