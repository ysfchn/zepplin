/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/** 
 * @import * as Types from "./_type.js"
 */

/*
    Zepplin uses pywebview module to display a webview from Python.

    pywebview exposes a object named in "pywebview" in window object to access Python
    functions in Javascript. However, this object is only available after "pywebviewready"
    event was called, so we resolve the Promise only when it is ready.

    See more:
    https://pywebview.flowrl.com/guide/interdomain.html#run-javascript-from-python
*/

function initPeer() {
    return new Promise((resolve) => {
        if (!Object.prototype.hasOwnProperty.call(window, "pywebview")) {
            window.addEventListener("pywebviewready", function () {
                resolve(window["pywebview"].api);
            }, { once: true });
        } else {
            resolve(window["pywebview"].api);
        }
    });
}

/**
 * @param {Types.CommandType} commandName
 * @param {Object | null} commandData 
 * @returns {Promise<object>}
 */
async function sendPythonCommand(commandName, commandData = null) {
    const currentPeer = this;
    /*
        Method names matched with their Python backend names.
        We are hardcoding these names even if both keys and values are same,
        so it allows updating them later more easily if a method name does
        change in either side.
    */
    const methodName = {
        "init": "init",
        "send_bluetooth": "send_bluetooth",
        "send_log": "send_log",
        "load_data": "load_data",
        "save_data": "save_data",
        "get_script": "get_script",
        "send_http": "send_http"
    }[commandName];
    return new Promise((resolve, reject) => {
        currentPeer[methodName](commandData || {}).then((output) => {
            if (methodName == "init") {
                resolve({
                    status: output.status
                });
            } else if (methodName == "load_data") {
                resolve({
                    user: output.user,
                    languages: output.languages,
                    app: output.app
                });
            } else if (methodName == "get_script") {
                resolve({
                    settings: output.settings,
                    side: output.side
                });
            } else if (methodName == "send_http") {
                resolve({
                    url: output.url,
                    status: output.status,
                    redirected: output.redirected,
                    headers: output.headers,
                    content: output.content
                });
            } else {
                resolve(null);
            }
        }).catch((reason) => reject(reason));
    });
}

/**
 * @type {Types.getPeer}
 */
export function getPeer() {
    return new Promise((resolve, reject) => {
        initPeer().then((peer) => {
            resolve(sendPythonCommand.bind(peer));
        })
        .catch((reason) => reject(reason));
    });
}