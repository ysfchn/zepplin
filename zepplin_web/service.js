/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * Creates a fake Response object that is compilant with Zepp mini-apps.
 * 
 * @param {string} url
 * @param {number} statusCode
 * @param {Object<string, string>} headers
 * @param {Object<string, any> | any[] | boolean | number | string} content
 */
function createHTTPResponse(url, statusCode, headers, content, isRedirected) {
    return Object.defineProperties(new Response(null, {
        status: statusCode,
        statusText: STATUS_CODE_MESSAGES[statusCode] || "",
        headers: new Headers(headers)
    }), {
        /*
            Not all attributes can be defined in the constructor, 
            so we override properties to return a specific value.
            https://developer.mozilla.org/en-US/docs/Web/API/Response/Response
        */
        url: { get: () => url },
        redirected: { get: () => isRedirected },
        /*
            Zepp's fetch() response object returns the body string directly,
            instead of a stream as in specification, so we repeat the same 
            here for compatibility.
        */
        body: { get: () => content },
        type: { get: () => "default" }
    })
}

/**
 * Fetch API
 * 
 * Creates a fetch()-like function for Zepp APIs that sends a HTTP request 
 * to the peer and returns a Promise with a Response object containing 
 * the HTTP response.
 * Exposed as "fetch" function.
 * 
 * https://docs.zepp.com/docs/reference/side-service-api/fetch/
 * 
 * @param {(...args) => Promise} peerCaller 
 * @returns {(...args : any[]) => Promise<Response>}
 */
function makeFetchPeerModule(peerCaller) {
    return (...args) => {
        return new Promise((resolve, reject) => {
            const isObject = (value) => typeof value === "object" && !Array.isArray(value);
            if (args.length == 0) {
                reject(new TypeError("fetch() requires at least a 1 argument"))
            }
            var requestParams = {};
            /*
                Allow calling fetch() with different signatures.
                fetch(URL, OPTIONS)
                fetch(URL)
                fetch(OPTIONS_CONTAINING_URL)
            */
            if (args.length == 1) {
                if (isObject(args[0]))
                    requestParams = args[0];
                else {
                    requestParams["method"] = "GET"
                    requestParams["url"] = String(args[0]);
                }
            } else if (isObject(args[1])) {
                requestParams = args[1];
            }
            if (!requestParams["method"]) {
                requestParams["method"] = "GET"
            }
            if (!requestParams["url"]) {
                reject(new TypeError("Can't send a request without URL is defined"))
            }
            /*
                Only "url", "body", "method" and "headers" are
                allowed for Zepp's fetch implementation.
            */
            for (const key of Object.keys(requestParams)) {
                if (!(["url", "body", "method", "headers"].includes(key))) {
                    delete requestParams[key];
                }
            }
            peerCaller("send_http", requestParams).then((response) => resolve(createHTTPResponse(
                response.url, response.status, response.headers, response.content, response.redirected
            ))).catch((reason) => reject(reason));
        })
    }
}

/**
 * Download File API
 * https://docs.zepp.com/docs/reference/side-service-api/download-file/
 * "network.downloader" namespace
 * 
 * @param {(...args) => Promise} peerCaller 
 */
function makeDownloadPeerModule(peerCaller) {
    const downloadFile = ({ url, timeout, headers = null, filePath = null }) => {
        return new Promise((resolve, reject) => {
            reject(new Error("Download File API is not implemented yet."));
        });
    }
    return Object.create(null, {
        downloadFile: {
            get: () => downloadFile
        }
    })
}

/**
 * Transfer File API
 * https://docs.zepp.com/docs/reference/side-service-api/transfer-file/
 * "tranfserFile" namespace
 * 
 * @param {(...args) => Promise} peerCaller 
 */
function makeTransferPeerModule(peerCaller) {
    const outboxObject = Object.create(null, {
        enqueFile: {
            get: () => ({ fileName = null, params = null }) => {
                throw new Error("Transfer File API is not implemented yet.")
            }
        }
    });
    const inboxObject = Object.create(null, {
        getNextFile: {
            get: () => () => {
                throw new Error("Transfer File API is not implemented yet.")
            }
        }
    });
    return Object.create(null, {
        getOnbox: {
            get: () => outboxObject
        },
        getInbox: {
            get: () => inboxObject
        }
    })
}

/**
 * Image Convert API
 * https://docs.zepp.com/docs/reference/side-service-api/image-convert/
 * "image" namespace
 * 
 * @param {(...args) => Promise} peerCaller 
 */
function makeImagePeerModule(peerCaller) {
    return Object.create(null, {
        convert: {
            get: () => ({ filePath, targetFilePath = null }) => {
                return Promise.reject(new Error("Image Convert API is not implemented yet."))
            }
        }
    })
}

/**
 * @param {(...args) => Promise} peerCaller 
 */
export function createServiceAPI(peerCaller) {
    return {
        fetch: makeFetchPeerModule(peerCaller),
        network: {
            downloader: makeDownloadPeerModule(peerCaller)
        },
        tranfserFile: makeTransferPeerModule(peerCaller),
        image: makeImagePeerModule(peerCaller)
    }
}

/*
    HTTP status codes mapped to their names.
    We use this to manually provide statusText for our fetch(), it is probably unlikely to happen 
    a mini-app checking for a status message instead of a status code, but we expose it anyway to 
    make it compatible with the original fetch() specification.

    https://developer.mozilla.org/en-US/docs/Web/API/Response/statusText

    Status messages are obtained from, excluding draft & unassigned:
    https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
*/
const STATUS_CODE_MESSAGES = {
    100: "Continue",
    101: "Switching Protocols",
    102: "Processing",
    103: "Early Hints",
    200: "OK",
    201: "Created",
    202: "Accepted",
    203: "Non-Authoritative Information",
    204: "No Content",
    205: "Reset Content",
    206: "Partial Content",
    207: "Multi-Status",
    208: "Already Reported",
    226: "IM Used",
    300: "Multiple Choices",
    301: "Moved Permanently",
    302: "Found",
    303: "See Other",
    304: "Not Modified",
    305: "Use Proxy",
    307: "Temporary Redirect",
    308: "Permanent Redirect",
    400: "Bad Request",
    401: "Unauthorized",
    402: "Payment Required",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    406: "Not Acceptable",
    407: "Proxy Authentication Required",
    408: "Request Timeout",
    409: "Conflict",
    410: "Gone",
    411: "Length Required",
    412: "Precondition Failed",
    413: "Content Too Large",
    414: "URI Too Long",
    415: "Unsupported Media Type",
    416: "Range Not Satisfiable",
    417: "Expectation Failed",
    421: "Misdirected Request",
    422: "Unprocessable Content",
    423: "Locked",
    424: "Failed Dependency",
    425: "Too Early",
    426: "Upgrade Required",
    428: "Precondition Required",
    429: "Too Many Requests",
    431: "Request Header Fields Too Large",
    451: "Unavailable For Legal Reasons",
    500: "Internal Server Error",
    501: "Not Implemented",
    502: "Bad Gateway",
    503: "Service Unavailable",
    504: "Gateway Timeout",
    505: "HTTP Version Not Supported",
    506: "Variant Also Negotiates",
    507: "Insufficient Storage",
    508: "Loop Detected",
    511: "Network Authentication Required"
};