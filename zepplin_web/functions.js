/*

    Copyright (C) 2024 ysfchn

    This file is part of Zepplin.

    Zepplin is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Zepplin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * @param {any} target 
 * @param  {...any} source 
 * @returns {any}
 */
export function initProxy(target, ...source) {
    if (!target) { 
        throw Error('target not undefined');
    }
    return new Proxy(target, {
        get(target, p) {
            for (const s of [target, ...source].reverse()) {
                if (!s) continue;
                if (p in s) return s[p];
            }
        },
        set(target, p, value) {
            target[p] = value;
            return true;
        },
    });
}

/**
 * @param {any} object 
 * @returns {string}
 */
export function dumpJSON(object) {
    try {
        return JSON.stringify(object)
    } catch (e) {
        return "[Circular]"
    }
}

/**
 * Returns a new function string that shadows all globals expect the
 * allowed symbols, and inserts the given function body. Used to run
 * the mini-app scripts.
 * 
 * "this" must be bound to window object.
 * 
 * @param {string} functionBody
 * @param {Object} exposedObjects
 * @returns {Function}
 */
export function createRunnerFunction(functionBody, exposedObjects) {
    const globalObject = {};
    for (const name of ALLOWED_SYMBOLS) {
        globalObject[name] = this[name];
    }
    const symbolRegex = /^[$_A-Z]\w*$/i;
    const shadowGlobals = Object.getOwnPropertyNames(this).filter(
        e => symbolRegex.test(e) && !(ALLOWED_SYMBOLS.includes(e))
    );
    Object.assign(globalObject, exposedObjects);
    const runnerFunction = new Function("locals",
        `(function (locals){ 
            var ${shadowGlobals.join(',')};
            var ${Object.keys(globalObject).map((e => `${e}=locals.${e}`)).join(',')};
            var globalThis = this;
            (function (){
                ${functionBody}
            }).call({});
        })(locals);`
    );
    return runnerFunction.bind(globalObject, globalObject);
}

/**
 * https://docs.zepp.com/docs/1.0/reference/side-service-api/fetch/
 * 
 * Zepp has own fetch() function which supports less properties than
 * original fetch, so we use a proxy function here to mimic the Zepp's
 * fetch() function.
 * 
 * TODO: Unused
 */
function fetchProxy() {
    /** @type {fetch} */
    const originalFetch = window.fetch.bind(window);
    return new Proxy(originalFetch, {
        apply: function (target, thisArg, argArray) {
            var fetchArg = argArray[0];
            if (typeof fetchArg === "string") {
                if (argArray.length > 1) {
                    // Also support common fetch syntax,
                    // fetch(URL, OPTIONS) 
                    fetchArg = {
                        url: fetchArg,
                        ...argArray[1]
                    }
                } else {
                    // fetch(URL)
                    fetchArg = { 
                        url: fetchArg, 
                        method: "GET"
                    }
                }
            }
            // Only "url", "body", "method" and "headers" are
            // allowed for Zepp's fetch implementation.
            for (const key of Object.keys(fetchArg)) {
                if (!(["url", "body", "method", "headers"].includes(key))) {
                    delete fetchArg[key];
                }
            }
            return new Promise((resolve, reject) => {
                target.apply(thisArg, [fetchArg.url, fetchArg])
                    .then((resp) => {
                        // Mini-apps expect the actual body in fetch(...).body,
                        // instead of getting a ReadableStream, so we read the 
                        // whole response value beforehand and pass it to the app.
                        resp.text().then((data) => {
                            resolve({
                                status: resp.status,
                                ok: resp.ok,
                                redirected: resp.redirected,
                                statusText: resp.statusText,
                                url: resp.url,
                                type: resp.type,
                                headers: resp.headers,
                                body: data,
                                bodyUsed: resp.bodyUsed
                            })
                        });
                    })
                    .catch((reason) => reject(reason))
            });
        }
    });
}

/**
 * @param {boolean} strict
 * @param {string} text
 * @param {any[]} parameters
 * @returns {string}
 */
export function formatText(strict, text, ...parameters) {
    let currentParameter = 0;
    const totalParameter = parameters.length;
    let output = String(text).replace(/%[sdj%]/g, (function (e) {
        if ("%%" === e)
            return "%";
        if (currentParameter >= totalParameter) 
            return e;
        switch (e) {
            case "%s":
                return String(parameters[currentParameter++]);
            case "%d":
                return Number(parameters[currentParameter++]).toString();
            case "%j":
                try {
                    return JSON.stringify(parameters[currentParameter++])
                } catch (Ef) {
                    return "[Circular]"
                }
            default:
                return e
        }
    }));
    if (strict) {
        return output;
    }
    for (let a = parameters[currentParameter]; currentParameter < totalParameter; a = parameters[++currentParameter]) {
        if ((null !== a) && ("object" === typeof a)) {
            output += " " + dumpJSON(a)
        } else {
            output += " " + a
        }
    }
    return output;
}

/**
 * @returns {string}
 */
export function getTime() {
    let now = new Date;
    const offset = now.getTimezoneOffset();
    now = new Date(now.getTime() - 60 * offset * 1e3);
    return now.toISOString();
}

/**
 * @param {Object<string, Object<string, string>>} langTable 
 * @param {Lang} currentLang 
 * @param {string} defaultLang 
 * @returns 
 */
export function getTextFactory(langTable, currentLang, defaultLang) {
    let msgTable = langTable[currentLang.getLang()] || langTable[defaultLang] || {};
    /**
     * @param {string} msgId
     * @param {any[]} args
     */
    function gettext(msgId, ...args) {
        return formatText(false, msgTable[msgId] || msgId, ...args);
    }
    gettext.locale = function (locale) {
        currentLang.setLang(locale);
        msgTable = langTable[currentLang.getLang()] || langTable[defaultLang] || {};
    };
    return gettext;
}

/**
 * Converts an ArrayBuffer to a hexadecimal string.
 * 
 * @param {ArrayBuffer} buffer
 * @returns {string}
 */
export function bufferToHex(buffer) {
    return [...new Uint8Array(buffer)].map(
        x => x.toString(16).padStart(2, "0")
    ).join("");
}

export class Lang {
    /**
     * @type {string} Language code, for example "en-US"
     */
    lang;

    /** @param {string} lang */
    constructor(lang) {
        this.lang = lang;
    }
    getLang() { 
        return this.lang;
    }
    setLang(lang) { 
        this.lang = lang;
    }
}

export class EventBus {
    /** @type {Map<string, Set<Function>>} */
    _registry;

    constructor() {
        this._registry = new Map();
    }

    /**
     * @param {string} eventName
     * @param {(...args) => any} callback 
     * @returns {this}
     */
    on(eventName, callback) {
        if (this._registry.has(eventName)) {
            const listeners = this._registry.get(eventName);
            if (listeners && listeners.has(callback))
                return this;
            listeners.add(callback);
        }
        else {
            this._registry.set(eventName, new Set().add(callback));
        }
        return this;
    }

    /**
     * @param {string} eventName
     * @param {(...args) => any} callback 
     * @returns {this}
     */
    once(eventName, callback) {
        const on = (...args) => {
            this.off(eventName, on);
            callback(...args);
        };
        this.on(eventName, on);
        return this;
    }

    /**
     * @param {string} eventName
     * @param {(...args) => any} callback 
     * @returns {this}
     */
    off(eventName, callback) {
        if (eventName) {
            if (callback) {
                const callbackList = this._registry.get(eventName);
                if (!callbackList)
                    return this;
                if (callbackList.has(callback)) {
                    callbackList.delete(callback);
                }
            }
            else {
                this._registry.delete(eventName);
            }
        }
        else {
            this._registry.clear();
        }
        return this;
    }

    /**
     * @param {string} eventName
     * @param {...any} args 
     * @returns {this}
     */
    emit(eventName, ...args) {
        const callbackList = this._registry.get(eventName);
        if ((callbackList !== undefined) && (callbackList !== null)) {
            for (const callback of callbackList) {
                callback && callback(...args);
            }
        }
        return this;
    }

    /**
     * @param {string} eventName
     * @returns {number}
     */
    count(eventName) {
        if (eventName) {
            const callbackList = this._registry.get(eventName);
            if ((callbackList !== undefined) && (callbackList !== null)) {
                return callbackList.size;
            }
            return 0;
        }
        return this._registry.size;
    }
}

export class SettingsStorage {
    /** @type {EventBus} */
    _eventBus;
    /** @type {Map<string, any>} */
    _data;

    /**
     * @param {EventBus} eventBus
     * @param {Object | null} data
     */
    constructor(eventBus, data = null) {
        this._eventBus = eventBus;
        this._data = new Map();
        if (typeof data === "object") {
            for (const [k, v] of Object.entries(data)) {
                this._data.set(k, v);
            }
        }
    }
    get length() {
        return this._data.size;
    }
    setItem(key, newValue) {
        const oldValue = this.getItem(key);
        this._data.set(key, newValue);
        this._eventBus.emit("refresh");
        this._eventBus.emit("refresh_change", {
            key: key, 
            oldValue: oldValue, 
            newValue: newValue
        });
    }
    getItem(key) {
        const value = this._data.get(key);
        return undefined === value ? null : value;
    }
    key(index) {
        const allKeys = [ ...this._data.keys() ];
        return index >= allKeys.length || index < 0 ? null : allKeys[index];
    }
    removeItem(key) {
        const wasExisted = this._data.delete(key);
        if (wasExisted) {
            this._eventBus.emit("refresh");
        }
    }
    clear() {
        const wasEmpty = this.length === 0;
        this._data.clear();
        if (!wasEmpty) {
            this._eventBus.emit("refresh");
        }
    }
    toObject() {
        const output = Object.create(null);
        for (const [key, value] of this._data.entries()) {
            output[key] = value;
        }
        return output;
    }
    addListener(eventName, callback) {
        // Event names must be "change".
        if (eventName != "change") {
            throw new Error(`All setting storage event names must be "change", not "${eventName}"!`);
        }
        this._eventBus.on("refresh_change", callback);
    }
}

const LOGGER_LEVELS = {
    all: 0,
    log: 1,
    debug: 2,
    info: 3,
    warn: 4,
    error: 5,
    off: 1000
};

export class Logger {
    /** @type {number} */
    static _appId = -1;
    /** @type {EventBus} */
    _eventBus;
    /** @type {number} */
    static _minimumLevel = LOGGER_LEVELS.all;
    /** @type {Object<string, Logger>} */
    static _loggers = {};

    /**
     * @param {string} name
     */
    constructor(name) {
        this.name = name;
        this.prefix = "";
        this.tag = "-";
        this._eventBus = new EventBus();
        this.on = this._eventBus.on.bind(this._eventBus);
        this.off = this._eventBus.off.bind(this._eventBus);
        Logger._loggers[this.name] = this;
    }

    /**
     * @param {number} minimumLevel
     */
    set level(minimumLevel) {
        Logger._minimumLevel = minimumLevel;
    }

    get level() {
        return Logger._minimumLevel;
    }

    /**
     * @param {string} name
     */
    getLogger(name) {
        return Logger._loggers[name] || Logger.getLogger(name);
    }

    /**
     * @param {string} name
     */
    static getLogger(name) {
        return new Logger(name.toString());
    }

    /**
     * @param {"log" | "debug" | "info" | "warn" | "error"} levelName
     * @param {string[]} log
     */
    stdout(levelName, ...log) {
        if (Logger._minimumLevel > LOGGER_LEVELS[levelName.toLowerCase()]) {
            return;
        }
        this._eventBus.emit("log", {
            level: levelName,
            appId: Logger._appId,
            timestamp: Date.now(),
            scope: this.prefix, // TODO: can be a function?
            name: this.name,
            tag: this.tag,
            message: log.join(" ")
        });
        console[levelName](...log);
    }
    
    /**
     * @param {Logger} logger 
     */
    connect(logger) {
        logger.on("log", (/** @type {Object} */ data) => {
            logger.log(JSON.stringify(data));
        });
    }

    /**
     * @param {string} _levelName 
     * @param {string} text 
     * @param  {...any} args 
     * @returns {string}
     */
    formatLog(_levelName, text, ...args) {
        let fields = [
            getTime(), _levelName.padEnd(5, " "), this.name, this.tag
        ];
        this.prefix && fields.unshift(this.prefix);
        return fields.map((v) => `[${v}]`).concat(...[formatText(false, text, ...args)]).join(" ");
    }

    log = this.stdout.bind(this, "log");
    debug = this.stdout.bind(this, "debug");
    info = this.stdout.bind(this, "info");
    warn = this.stdout.bind(this, "warn");
    error = this.stdout.bind(this, "error");
}

const ALLOWED_SYMBOLS = [
    "Infinity", 
    "NaN", 
    "undefined", 
    "isFinite", 
    "isNaN", 
    "parseFloat", 
    "parseInt", 
    "encodeURIComponent", 
    "decodeURIComponent", 
    "Object", 
    "Array", 
    "Boolean", 
    "Symbol", 
    "String", 
    "Number", 
    "Date", 
    "RegExp", 
    "Error", 
    "RangeError", 
    "ReferenceError", 
    "TypeError", 
    "URIError", 
    "Promise",
    "Map",
    "Set",
    "Headers",
    "Request",
    "Response",
    "JSON",
    "Math",
    "setTimeout",
    "clearTimeout",
    "setInterval",
    "clearInterval",
    "Navigator",
    "navigator"
];