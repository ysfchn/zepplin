#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from enum import Enum
from io import BytesIO
from pathlib import Path
from subprocess import Popen, DEVNULL, PIPE, STDOUT
from threading import Thread
from typing import IO, List, Optional, Tuple, Union, Unpack, cast
from zipfile import ZipFile
import sys
import signal
from base64 import b64decode

from zepplin.constants import (
    _VarInfo,
    BRIDGE_CLIENT_SIMULATOR,
    EMULATOR_WS_FORWARD_ADDR,
    EMULATOR_WS_ORIGIN_ADDR,
    EMULATOR_WS_ORIGIN_MASK,
    EMULATOR_WS_COMMANDS_URL,
    EMULATOR_WS_BLUETOOTH_URL,
    EMULATOR_WS_BLUETOOTH_SUBPROTOCOL,
    REGEX_ANSI_ESCAPE,
    REGEX_LOG_CAPTURE,
    EMULATOR_WS_PAYLOAD_SUCCESS,
    EMULATOR_WS_PAYLOAD_ERROR,
    EmulatorSystemStatuses
)
from zepplin.api.bridge import BridgeBase
from zepplin.api.account import Account
from zepplin.logging import get_logger
from zepplin.utils import app_id_to_hex, make_qemu_args

from websockets.sync.client import connect, ClientConnection
from websockets.protocol import State
from websockets.exceptions import ConnectionClosed
import orjson
from urllib.request import urlopen
from urllib.error import URLError

class Emulator:
    """
    Emulate Zepp OS firmware in QEMU to debug mini-apps.

    This class does these things:
    
    -   Launch the given firmware file in given QEMU executable path. Therefore a QEMU binary that
        is compiled to emulate "mps2-an521" machines and "cortex-m33" as CPU is required. QEMU's source
        code has a hard-coded RAM limit on "mps2-an521" machines, which is too low to boot Zepp OS, so a
        binary that is compiled on a modified QEMU source code that doesn't have that limit is required 
        for Zepp OS.
    
    Zepp OS firmware images starts two websocket servers on localhost on boot to allow sending/receiving
    commands between guest (QEMU) and host (our computer). Therefore this class will also initialize a websocket 
    client to connect both of these communication channels.

    -   First websocket server (at "ws://127.0.0.1:7833/SIMULATOR") accepts system commands payloads, such as
        installing an app, removing an app, changing system statuses, taking a screenshot and so on.
    
    -   Second websocket server (at "ws://127.0.0.1:7833") is used for sending abritary messages between
        mini-apps and side-apps. It is also referred as Bluetooth channel, but since we are not on a real device,
        there is no actual Bluetooth connection has made.

    This class will automatically manage the connection to both of the websockets, so no need to connect 
    to the emulator websocket server manually.
    """

    __slots__ = (
        "_thread_qemu",
        "_thread_ping",
        "_thread_bluetooth",
        "_process_qemu",
        "_websocket_commands",
        "_websocket_bluetooth",
        "_path_qemu",
        "_path_kernel",
        "_path_disk",
        "_logger"
    )

    def __init__(
        self, 
        qemu_bin : Path, 
        kernel_file : Path, 
        storage_file : Path
    ) -> None:
        self._thread_qemu : Optional[Thread] = None
        self._thread_ping : Optional[Thread] = None
        self._thread_bluetooth : Optional[Thread] = None
        self._process_qemu : Optional[Popen] = None
        self._websocket_commands : Optional[ClientConnection] = None
        self._websocket_bluetooth : Optional[ClientConnection] = None
        self._path_qemu : str = None # type: ignore
        self._path_kernel : str = None # type: ignore
        self._path_disk : str = None # type: ignore
        self._set_paths(qemu_bin, kernel_file, storage_file)
        self._logger = get_logger("emulator")

    def start(self, register_signals : bool = True):
        """
        Start the emulator and create required connections.
        """
        if register_signals:
            signal.signal(signal.SIGINT, self._stop_from_signal)
            signal.signal(signal.SIGTERM, self._stop_from_signal)
        self._thread_qemu = Thread(target = self._start_qemu)
        self._thread_qemu.start()
        self._thread_ping = Thread(target = self._start_ping)
        self._thread_ping.start()

    def stop(self):
        """
        Stop the emulator and close all connections.
        """
        if not self._process_qemu:
            raise Exception("Emulator is not initialized yet!")
        self._process_qemu.terminate()
        self._release()

    def wait(self):
        """
        Block until emulator stops.
        """
        if not self._thread_qemu:
            raise Exception("Emulator is not initialized yet!")
        self._thread_qemu.join()

    def wait_for_commands(self):
        """
        Block until an connection has estahbilished with emulator so it can 
        accept the incoming commands. Raises an Exception if emulator has been stopped.
        """
        if not self._thread_ping:
            raise Exception("Emulator is not initialized yet!")
        self._thread_ping.join()
        if not self._websocket_commands:
            raise Exception("Emulator has been killed while waiting for commands")

    def command_screen_shot(self) -> Optional[bytes]:
        """
        Take a screenshot of the device and return the image as bytes (in PNG format?). 
        The returned image is not rounded regardless of the device shape, so you may want 
        to obtain the device frame and use as a mask.
        
        Only devices with Zepp OS version 2.0 and above seems to support this command,
        so if the device doesn't acknowledge the screenshot command, this function will 
        return None instead.
        """
        data = self._ws_send_command_payload("ScreenShot")
        # If command is not supported on the machine, it will return b"Error!" message.
        if data == EMULATOR_WS_PAYLOAD_ERROR:
            # TODO: Maybe take the screenshot of QEMU window as an alternative?
            return None
        # Otherwise, it will be a JSON payload that contains base64 string of the image.
        # { "method": "ScreenShot", "length": 188327, "file": "<base64 string>" }
        payload = orjson.loads(data)
        return b64decode(payload["file"])

    def command_screen_off(self) -> bool:
        """
        Turn off the screen.
        Returns True on success, else False.
        """
        return self._ws_send_command_payload("ScreenRest") == EMULATOR_WS_PAYLOAD_SUCCESS

    def command_remove_app(self, app_id : Optional[int] = None) -> bool:
        """
        Remove the installed app by its ID from emulator.
        Returns True on success, else False.
        """
        extra = {}
        if app_id:
            extra["appid"] = app_id_to_hex(app_id)
        return self._ws_send_command_payload(
            "Project",
            type = "remove" if app_id else "clear",
            **extra
        ) == EMULATOR_WS_PAYLOAD_SUCCESS

    def command_install_app(self, app_package : Union[IO[bytes], Path], app_id : int) -> bool:
        """
        Install the given app package (from Path or file-object) to the emulator.
        Returns True on success, else False.
        """
        if isinstance(app_package, Path):
            buffer = BytesIO()
            buffer.write(app_package.read_bytes())
            buffer.seek(0)
            return self.command_install_app(buffer, app_id)
        # Check if this is a .zpk file, and extract the inner app if that so.
        zipf = ZipFile(app_package, mode = "r")
        app_archive = None
        if "device.zip" in zipf.namelist():
            app_archive = zipf.read("device.zip")
        zipf.close()
        if not app_archive:
            app_package.seek(0)
            app_archive = app_package.read()
        return self._ws_send_command_payload(
            "Project",
            type = "install",
            appid = app_id_to_hex(app_id),
            length = len(app_archive),
            # Insert the bytes to the JSON without any type of encoding,
            # and wrap the value with quote (") characters (byte: 34), because
            # emulator kernel expects it like that.
            file = orjson.Fragment(bytes([34, *app_archive, 34])) 
        ) == EMULATOR_WS_PAYLOAD_SUCCESS

    def command_open_app(self, app_id : int, is_watchface : bool = False) -> bool:
        """
        Open an application on the emulator without user interaction by its app ID. 
        If `is_watchface` is True, then the given watchface ID will be applied instead.

        Returns True on success, else False.
        """
        return self._ws_send_command_payload(
            "OpenProject" if not is_watchface else "ChangeWatchFace",
            appid = app_id_to_hex(app_id)
        ) == EMULATOR_WS_PAYLOAD_SUCCESS

    def command_set_config(self, **config : Unpack[EmulatorSystemStatuses]) -> bool:
        """
        Change system status values, such as battery level, weather, calorie
        and more from given keyword arguments. See `EmulatorSystemStatuses` for all
        available options and their accepted values. If a status key has not provided, 
        then its default value will be used.

        Note that while the payload is the same for all Zepp OS devices, some devices 
        may handle this differently, for example it might be needed to open the app 
        list to make these values to take an effect, or the device itself may not 
        change the status at all.

        Returns True on success, else False.
        """
        full_config = bytearray()
        type_names = {int: "int32", str: "string", bool: "bool"}
        for key, ann in EmulatorSystemStatuses.__annotations__.items():
            # Get the given config value from kwargs, if key doesn't exists,
            # get the default value from typing.Annotated metadata parameters.
            expected_type, field_info = ann.__args__[0], cast(_VarInfo, ann.__metadata__[0]) # noqa: E501
            value = config.get(key, None)
            if value is None:
                value = field_info.default
            # Check if value is in allowed range.
            if expected_type == int:
                value = int(value) # type: ignore
                if (field_info.le is not None) and (value > field_info.le):
                    raise ValueError(f"Can't set '{key}' higher than {field_info.le}.")
                if (field_info.ge is not None) and (value < field_info.ge):
                    raise ValueError(f"Can't set '{key}' lower than {field_info.ge}.")
                if (field_info.enum is not None) and ((value > len(field_info.enum)) or (value < 0)): # noqa: E501
                    raise ValueError(f"'{key}' must be a value between 0 and {len(field_info.enum)}.") # noqa: E501
            elif expected_type == bool:
                value = "true" if bool(value) else "false"
            elif issubclass(expected_type, Enum):
                # TODO: Better handle enums
                allowed_values = []
                if field_info.enum:
                    allowed_values = field_info.enum
                else:
                    allowed_values = list(expected_type)
                if value not in allowed_values:
                    raise ValueError(f"'{value} is not an allowed value for '{key}'.")
                value = value.value # type: ignore
            full_config.extend("vcfg_{0}|{1}|{2}\r\n".format(
                key, 
                type_names[expected_type], # type: ignore
                str(value)
            ).encode("ascii"))
        return self._ws_send_command_payload(
            "Config",
            # Don't escape \r\n characters, insert it to JSON directly without caring,
            # and wrap the value with quote (") characters (byte: 34), because
            # emulator kernel expects it like that.
            vconfig = orjson.Fragment(bytes([34, 123, *full_config, 125, 34]))
            # Output (including quotes):
            # "{vcfg_battery|int32|88\r\nvcfg_mute|bool|true}"
        ) == EMULATOR_WS_PAYLOAD_SUCCESS

    def _build_qemu_args(self, use_spice : bool = False):
        display_args : Optional[List[Union[str, List[str]]]] = None
        additional_args = dict()
        if use_spice:
            display_args = ["spice-app"]
            additional_args["spice"] = [
                ["disable-ticketing", "on"],
                ["disable-copy-paste", "on"],
                ["gl", "off"]
            ]
        elif sys.platform == "darwin":
            display_args = ["cocoa", ["show-cursor", "on"]]
        else:
            display_args = ["gtk", ["show-cursor", "on"]]
        return make_qemu_args({
            "name": "zepplin-qemu",
            "machine": ["mps2-an521", ["accel", "tcg"]],
            "cpu": "cortex-m33",
            "m": "128M",
            "drive": [
                ["file", self._path_disk],
                ["bus", "0"],
                ["unit", "0"],
                ["format", "raw"],
                ["if", "none"],
                ["media", "disk"]
            ],
            "kernel": self._path_kernel,
            "display": display_args,
            "serial": "stdio",
            # Prevent norflash files being modified with "-snapshot"
            "snapshot": None,
            # Expose the websocket server that launched in virtual machine to our localhost, 
            # so we can communicate with the emulator's websocket server.
            "net[0]": [
                "user",
                ["ipv6", "off"],
                # TODO: Just put the port instead of IP?
                ["hostfwd", f"tcp:{EMULATOR_WS_FORWARD_ADDR}-{EMULATOR_WS_ORIGIN_ADDR}"],
                ["net", EMULATOR_WS_ORIGIN_MASK],
                ["restrict", "y"]
                # ["model", "lan9118"]
            ],
            "net[1]": "nic",
            # Reboots doesn't work anyway, so we can disable the reboot button on GUI.
            "no-reboot": None,
            "nodefaults": None,
            # Zepp OS won't boot without semihosting flag.
            "semihosting": None,
            **additional_args
        }) # type: ignore

    def _release(self):
        if self._websocket_bluetooth:
            self._websocket_bluetooth.close()
        if self._websocket_commands:
            self._websocket_commands.close()

    def _stop_from_signal(self, signal, frame):
        self.stop()

    @staticmethod
    def _parse_log(log : bytes) -> Union[Tuple[str, str, str, str, int], Tuple[str]]:
        levels = ["L", "D", "I", "W", "E"]
        levels_names = ["log", "debug", "info", "warn", "error"]
        clean_log = REGEX_ANSI_ESCAPE.sub(b"", log.strip())
        matched = REGEX_LOG_CAPTURE.match(clean_log)
        if matched:
            groups = matched.group(
                "time", "severity", "source", "module", "message",
                "j_prefix", "j_datetime", "j_severity", "j_name", "j_tag"
            )
            groups_text : List[Optional[str]] = []
            for g in groups:
                if g is None:
                    groups_text.append(None)
                else:
                    groups_text.append(g.decode("utf-8", "replace"))
            # If this is a JSFWK-formatted log:
            is_jsfwk = all(groups_text[5:])
            return (
                groups_text[4],
                groups_text[0] if not is_jsfwk else groups_text[6],
                # TODO: Investigate why "groups_text[3]" is sometimes None.
                groups_text[3] if not groups_text[2] else (groups_text[2] + "." + (groups_text[3] or "UNKNOWN")),
                "" if not is_jsfwk else (groups_text[8] + "." + groups_text[9]), # type: ignore
                (levels.index(groups_text[1]) if not is_jsfwk else levels_names.index(groups_text[7])) * 10, # type: ignore
            )
        return (
            clean_log.decode("utf-8", "replace"),
        )

    def _start_qemu(self):
        self._process_qemu = Popen(
            args = [self._path_qemu, *self._build_qemu_args()],
            stdin = DEVNULL, 
            stdout = PIPE, 
            stderr = STDOUT
        )
        system_logger = self._logger.getChild("system")
        for line in iter(self._process_qemu.stdout.readline, b""): # type: ignore
            parsed = self._parse_log(line)
            if len(parsed) == 1:
                if parsed[0]:
                    system_logger.log(10, parsed[0])
            else:
                system_logger.log(
                    level = parsed[4], 
                    msg = parsed[2] + " | " + ("" if not parsed[3] else "(" + parsed[3] + ") ") + parsed[0], 
                    stacklevel = 0
                )
        if self._process_qemu.wait() != 0:
            raise Exception(f"QEMU command exited with {self._process_qemu.returncode} code.")
        self._release()

    def _start_ping(self):
        is_ready = False
        while not is_ready:
            try:
                if not self._thread_qemu:
                    break
                if not self._thread_qemu.is_alive():
                    break
                req = urlopen("http://" + EMULATOR_WS_FORWARD_ADDR, timeout = 1)
                if req.code == 200:
                    is_ready = True
            except (URLError, TimeoutError, ConnectionResetError, ):
                pass
        if not is_ready:
            return
        self._websocket_commands = connect(uri = EMULATOR_WS_COMMANDS_URL)
        self._websocket_bluetooth = connect(
            uri = EMULATOR_WS_BLUETOOTH_URL, 
            subprotocols = (EMULATOR_WS_BLUETOOTH_SUBPROTOCOL, ), # type: ignore
            max_size = None
        )
        self._thread_bluetooth = Thread(target = self._start_bluetooth)
        self._thread_bluetooth.start()

    def _start_bluetooth(self):
        while True:
            try:
                self._bluetooth_received(self._websocket_bluetooth.recv()) # type: ignore
            except ConnectionClosed:
                break

    def _bluetooth_received(self, data : bytes):
        # no-op, meant to be subclassed.
        ...

    def _bluetooth_send(self, data : bytes):
        if not self._websocket_bluetooth:
            raise Exception("Websocket is not connected.")
        self._websocket_bluetooth.send(data)

    def _is_connected(self):
        ble, mng = False, False,
        if self._websocket_bluetooth:
            if self._websocket_bluetooth.protocol.state == State.OPEN:
                ble = True
        if self._websocket_commands:
            if self._websocket_commands.protocol.state == State.OPEN:
                mng = True
        return ble and mng

    def _ws_send_command_payload(self, _method : str, /, **data) -> bytes:
        if not self._is_connected():
            raise Exception("Websocket is not connected.")
        payload = orjson.dumps({"method": _method, **data}, option = None)
        self._websocket_commands.send(payload) # type: ignore
        return self._websocket_commands.recv() # type: ignore

    def _set_paths(
        self,
        qemu_bin : Path, 
        kernel_file : Path, 
        storage_file : Path
    ):
        if not qemu_bin.exists():
            raise Exception(f"QEMU binary doesn't exists at path, {qemu_bin}")
        if not kernel_file.exists():
            raise Exception(f"Kernel file doesn't exists at path, {kernel_file}")
        if not storage_file.exists():
            raise Exception(f"Storage file doesn't exists at path, {storage_file}")
        self._path_qemu = str(qemu_bin.resolve())
        self._path_kernel = str(kernel_file.resolve())
        self._path_disk = str(storage_file.resolve())


class EmulatorBridge(Emulator):
    """
    Same as `Emulator`, but this class will also connect to the Developer Bridge with given Zepp account, 
    so it will be discoverable and will work on Zepp-related services such as Watchface Maker.
    """

    __slots__ = (
        "_bridge"
    )

    def __init__(
        self, 
        qemu_bin: Path, 
        kernel_file: Path, 
        storage_file: Path,
        account : Account
    ) -> None:
        super().__init__(qemu_bin, kernel_file, storage_file)
        def get_client_name(_): return BRIDGE_CLIENT_SIMULATOR
        def delete_package(_, app_id): self.command_remove_app(app_id)
        def take_screenshot(_): self.command_screen_shot()
        def install_package(_, app_package, app_id, is_watchface):
            self.command_install_app(app_package, app_id)
            self.command_open_app(app_id, is_watchface)
        bridge_class = type("EmulatorBridge", (BridgeBase, ), {
            "get_client_name": get_client_name,
            "take_screenshot": take_screenshot,
            "delete_package": delete_package,
            "install_package": install_package,
        })
        self._bridge = bridge_class(account)

    def start(self, register_signals : bool = True):
        super().start(register_signals = register_signals)
        self._bridge.connect()

    def stop(self):
        super().stop()
        self._bridge.close()