# `emulator`

This sub-package contains code to download and emulate Zepp OS firmware images on [QEMU](https://www.qemu.org/), like how [Zepp Simulator](https://docs.zepp.com/docs/1.0/guides/tools/simulator/) does.

The most important classes and methods in this package:

* `zepplin.emulator.classes.Emulator`
* `zepplin.emulator.classes.EmulatorBridge`

See [`zepplin.api.account.Account:get_emulator_list`](../api/account.py) method to download emulator images. Unlike other account related methods, this method doesn't require account credentials, since its endpoint is public.

Table of contents:

* [Emulation](#emulation)
* [Implementation](#implementation)

## Emulation

_See README in [`qemu`](../../qemu/)._

## Implementation

> This is an explanation of "how Zepp did it", and comparison to its implementation in Zepplin, so while I try to explain as much as possible, it may still not contain every piece of detail that is found while reverse-engineering Zepp apps.

Zepp Simulator downloads a JSON from URL listed below to get list of available device machines with their URLs to download the machine kernel (`main.elf`) and storage (`norflash.bin`) file.

Production: <https://upload-cdn.huami.com/zeppos/simulator/download/emulatorList.json><br>
Test: <https://upload-testing-cdn.huami.com/zeppos/simulator/download/emulatorList.json>

Zepp OS kernel image starts a local websocket server after boot to create a communication channel between guest and host, so it lets us to push commands to the machine. The websocket server inside the guest listens at `192.168.166.188:7833`, regardless of the actual IP assigned to the virtual machine. Thus it requires us to launch QEMU with required flags to make connecting to the websocket from the host possible.

There is also a second websocket server which allows mini-apps to send packages to side-apps.

[Zepp OS documentation](https://docs.zepp.com/docs/1.0/guides/tools/simulator/setup/#environmental-preparation) suggests to install tun/tap (virtual network card) to the host device and configure its IP same with websocket server. But as this process requires configuration and admin privileges on the host system, I figured out that it is also possible to just forward the address to host's localhost, so we can avoid any type of manual configuration in Zepplin:

```bash
# hostfwd=... - host forward rule: 127.0.0.1:7833 <-> 192.168.166.188:7833
# net=... - set the visible address of guest
-net user,ipv6=off,hostfwd=tcp:127.0.0.1:7833-192.168.166.188:7833,net=192.168.166.1/24,restrict=y
```

> See `_build_qemu_args` method in `Emulator` class for the full QEMU command line arguments used to boot the machine.

Aside from machine itself, Zepp Simulator app also starts a socket.io server at port `7650` to communicate with CLI, but it is unimplemented in Zepplin since the its only purpose is to upload applications from CLI -> Zepp Simulator -> and then to the virtual Zepp OS machine.