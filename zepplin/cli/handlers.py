from zepplin.api.account import Account
from pathlib import Path
from typing import NamedTuple
from rich.console import Console
from rich.table import Table
from datetime import datetime

class CLIContext(NamedTuple):
    app_dir : Path

def login_account(
    config : CLIContext,
    is_xiaomi : bool, 
    password : str, 
    email : str
):
    is_password = (password and email)
    if is_xiaomi and (password or email):
        raise ValueError("Both --xiaomi and --password/--email can't be provided!")
    elif (password or email) and (not is_password):
        raise ValueError("If trying to login with a password, both --password and --email must be provided!")
    account = None
    if is_xiaomi:
        print("Signing in with a Xiaomi account...")
        account = Account.login_xiaomi()
    elif is_password:
        print("Signing in with password and email...")
        account = Account.login_password(email, password)
    else:
        print("Signing in on browser...")
        account = Account.login_flow()
    file = config.app_dir / "zepplin.json"
    account.to_file(file)
    print("Credentials has saved to:", str(file))

def logout_account(
    config : CLIContext
):
    file = config.app_dir / "zepplin.json"
    if not file.exists():
        print("No credentials was found to logout from, exiting...")
        exit(1)
    account = Account.from_file(file)
    print(account.logout())
    file.unlink(missing_ok = True)

def status_account(
    config : CLIContext
):
    zepplin_data = config.app_dir / "zepplin.json"
    with Console() as console:
        if not zepplin_data.exists():
            console.print("[bright_black]:key:[orange3] Not signed in.[/][/]")
            console.print(f"[bright_black]No credentials were found at {zepplin_data}.[/]")
        else:
            account = Account.from_file(zepplin_data)
            expiration = datetime.fromtimestamp(account.session.expiration / 1000, datetime.now().tzinfo) \
                .strftime("%d %b %Y, %H:%M")
            if account.is_expired:
                console.print("[bright_black]:key:[orange3] Credentials expired, login again.[/][/]")
                console.print(f"[bright_black]Credentials were found at {zepplin_data}.[/]")
                console.print(f"[bright_black]Expired at [bold]{expiration}[/].[/]")
            else:
                console.print("[bright_black]:key:[green3] Signed in.[/][/]")
                console.print(f"[bright_black]Credentials were found at {zepplin_data}.[/]")
                console.print(f"[bright_black]Will expire on [bold]{expiration}[/].[/]")

def list_emulators(
    config : CLIContext,
    include_test : bool
):
    emulators = Account.get_emulator_list(True)
    if include_test:
        emulators.extend(Account.get_emulator_list(False))
    with Console() as console:
        table = Table("ID", "Codename", "Name", "Targets", "Screen ratio", "Last version")
        for i in emulators:
            table.add_row(
                i.id, i.codename or "[bright_black]N/A[/]", i.display_name,
                ", ".join([str(x) for x in i.device_source]), f"{i.screen_ratio[0]} * {i.screen_ratio[1]}",
                "[bright_red]No releases.[/]" if not i.releases else i.releases[0].version_code,
                style = None if i.releases else "strike"
            )
        console.print(table)