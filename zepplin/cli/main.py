from pathlib import Path
from typing import Optional

from zepplin.cli.handlers import (
    list_emulators,
    login_account, 
    logout_account, 
    status_account,
    CLIContext
)

import click

def get_zepplin_folder(custom_path : Optional[str]):
    path = Path(custom_path or click.get_app_dir("zepplin")).resolve()
    if not path.exists():
        if custom_path:
            click.secho("Custom app path doesn't exists: " + str(path), err = True, fg = "red")
            exit(1)
        else:
            click.secho("Creating data folder at: " + str(path), fg = "yellow")
            path.mkdir()
    return path

@click.group(no_args_is_help = True)
@click.option("--app-dir", help = "Application directory")
@click.pass_context
def cli(ctx : click.Context, app_dir : Optional[str]):
    ctx.obj = CLIContext(
        app_dir = get_zepplin_folder(app_dir)
    )

# ---------------------------------------------------------------------------------
# Account
# ---------------------------------------------------------------------------------

@cli.group(help = "Manage your Zepp/Xiaomi account.")
def account():
    pass

@account.command(help = "Login with your Zepp account.")
@click.option(
    "--xiaomi", "-x", 
    default = False, flag_value = True,
    help = "Login with your Xiaomi account instead and link to Zepp."
)
@click.option("--password", "-p", type = click.STRING)
@click.option("--email", "-e", type = click.STRING)
@click.pass_obj
def login(obj : CLIContext, xiaomi : bool, password : str, email : str):
    login_account(
        config = obj,
        is_xiaomi = xiaomi,
        password = password,
        email = email
    )

@account.command(help = "Logout from your account.")
@click.pass_obj
def logout(obj : CLIContext):
    logout_account(
        config = obj
    )

@account.command(help = "Check if user is signed in.")
@click.pass_obj
def status(obj : CLIContext):
    status_account(
        config = obj
    )

# ---------------------------------------------------------------------------------
# Emulator
# ---------------------------------------------------------------------------------

@cli.group(help = "Manage emulators.")
def emulator():
    pass

@emulator.command(help = "Login with your Zepp account.")
@click.option(
    "--test", "-t", 
    default = False, flag_value = True,
    help = "Include test (non-production) emulators."
)
@click.pass_obj
def download(obj : CLIContext, test : bool):
    list_emulators(obj, include_test = test)