#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from enum import StrEnum
from io import BytesIO
from typing import IO, Annotated, Any, Dict, List, Literal, Optional, Tuple, Union, cast, final
from threading import Thread
from abc import ABC, abstractmethod
from urllib.parse import urlencode
from time import time
import signal

from pydantic import BaseModel, ConfigDict, Discriminator, Field, RootModel, Tag
from pydantic.alias_generators import to_camel

from zepplin.logging import get_logger
from zepplin.api.account import Account
from zepplin.constants import BRIDGE_CLIENT_TYPES, USER_AGENT_NODEFETCH

import httpx
from websockets import ConnectionClosed
from websockets.sync.client import connect, ClientConnection


class BridgeMethod(StrEnum):
    CLIENT = "sysEventPush"
    MESSAGE = "sysMessagePush"
    SETTING = "sysSettingPush"
    SCREENSHOT = "screenShot"
    CONNECT = "connectClient"
    LOG = "logPush"
    PACKAGE = "packagePush"

class BridgeEvent(StrEnum):
    CLIENT_CHANGE = "clientChange"
    SETTING_SCREENSHOT = "screenshot"
    SETTING_DELETE = "delete"


class BridgeBase(ABC):
    """
    Implementation of developer bridge that creates a communication channel 
    between various Zepp tools.
    https://docs.zepp.com/docs/1.0/guides/faq/developer-bridge-mode/
    """
    __slots__ = (
        "account",
        "_client_type",
        "_websocket",
        "_websocket_thread",
        "_clients",
        "_logger"
    )

    def __init__(self, account : Account) -> None:
        self.account = account
        self._client_type = self.get_client_name()
        self._websocket : Optional[ClientConnection] = None
        self._websocket_thread : Optional[Thread] = None
        self._clients : List[BridgeClientModel] = []
        self._logger = get_logger("bridge")

    @property
    @final
    def clients(self):
        return self._clients

    @property
    @final
    def me(self):
        for n in self._clients:
            if n.itself:
                return n

    @abstractmethod
    def get_client_name(self) -> Tuple[BRIDGE_CLIENT_TYPES, str]:
        raise NotImplementedError()

    def client_update(self) -> None:
        pass

    def save_screenshot(self, image : bytes) -> None:
        pass

    @abstractmethod
    def take_screenshot(self) -> bytes:
        raise NotImplementedError()

    @abstractmethod
    def delete_package(self, app_id : str) -> None:
        raise NotImplementedError()

    @abstractmethod
    def install_package(self, app_package : IO[bytes], app_id : int, is_watchface : bool) -> None:
        raise NotImplementedError()

    @abstractmethod
    def connect_client(self, client_id : str) -> None:
        raise NotImplementedError()

    @final
    def connect(self):
        _type, _name = self._client_type
        self._connect(_type, _name)

    @final
    def close(self):
        if self._websocket:
            self._websocket.close(1001)
            self._logger.log(20, "Disconnected")

    @final
    def _stop_from_signal(self, signal, frame):
        self._logger.log(10, f"Received signal {signal}, closing")
        self.close()

    @final
    def _receive_payload(self, data : str):
        assert self._websocket
        self._logger.log(10, "Receive: " + data)
        # Heartbeat, this is the only payload that is not encoded with JSON.
        if data == "ping":
            self._send_data("pong")
            return
        payload = BridgePayloadModel.model_validate_json(data).root

        if isinstance(payload, BridgePayloadErrorModel):
            self._logger.log(40, f"Error code {payload.error.code}: {payload.error.message}")
            return

        # Update clients list
        if payload.method == BridgeMethod.CLIENT:
            payload = cast(BridgePayloadClientModel, payload)
            self._logger.log(20, f"Updated clients list with {len(payload.params.clients)} clients.")
            self._clients.clear()
            for client in payload.params.clients:
                self._clients.append(client)
            self.client_update()

        # Flag the specified client as "connected", doesn't appear
        # to be returning any payload.
        elif payload.method == BridgeMethod.CONNECT:
            payload = cast(BridgePayloadConnectModel, payload)
            self._logger.log(20, f"Marked as connected to {payload.params.client_id}")

        # Take screenshot or delete package.
        elif payload.method == BridgeMethod.SETTING:
            payload = cast(BridgePayloadSettingModel, payload)
            if payload.params.type == BridgeEvent.SETTING_SCREENSHOT:
                self._reply_result(
                    BridgeMethod.SCREENSHOT,
                    self.take_screenshot().hex(),
                    payload.id
                )
            elif payload.params.type == BridgeEvent.SETTING_DELETE:
                self._logger.log(20, f"Deleting app with ID {payload.params.app_id}")
                self.delete_package(payload.params.app_id)
                self._reply_payload(
                    BridgeMethod.MESSAGE,
                    payload.id,
                    type = "notice",
                    title = "",
                    content = "uninstall success",
                    time = int(time() * 1000)
                )

        # Save the incoming screenshot.
        elif payload.method == BridgeMethod.SCREENSHOT:
            payload = cast(BridgePayloadResultModel, payload)
            assert type(payload.result) == str # noqa: E721
            self.save_screenshot(bytes.fromhex(payload.result))

        # Download an app from given URL and install it.
        elif payload.method == BridgeMethod.PACKAGE:
            payload = cast(BridgePayloadPackageModel, payload)
            buffer = self._download_package(payload.params.url, payload.params.app_type == "watchface")
            self.install_package(buffer, payload.params.app_id, payload.params.app_type == "watchface")
            self._reply_payload(
                BridgeMethod.MESSAGE,
                payload.id,
                type = "notice",
                title = "",
                content = "install success",
                time = int(time() * 1000)
            )

        elif payload.method == BridgeMethod.LOG:
            payload = cast(BridgePayloadLogModel, payload)
            self._logger.log(10, f"{payload.params.level.upper()} {payload.params.message}")

        elif payload.method == BridgeMethod.MESSAGE:
            payload = cast(BridgePayloadMessageModel, payload)
            self._logger.log(10, "Message: " + payload.params.content)

        else:
            raise NotImplementedError(f"Unhandled bridge payload: {payload.model_dump_json()}")

    @final
    def _pull_ws(self, url : str):
        self._websocket = connect(
            uri = url, user_agent_header = None
        )
        self._logger.log(20, "Connected")
        try:
            def _pull_data():
                data = self._websocket.recv() # type: ignore
                assert isinstance(data, str)
                self._receive_payload(data)
                _pull_data()
            _pull_data()
        except ConnectionClosed:
            pass

    @final
    def _download_package(self, url : str, is_watchface : bool):
        # Headers seem to be added only if package is not a watchface.
        headers = {
            "User-Agent": USER_AGENT_NODEFETCH
        }
        if not is_watchface:
            headers["apptoken"] = self.account.session.app_token
        buffer = BytesIO()
        self._logger.log(10, f"Downloading package: {url}, with headers {headers}")
        with httpx.stream("GET", url, headers = headers) as req:
            for i in req.iter_raw(2048):
                buffer.write(i)
        buffer.seek(0)
        return buffer

    @final
    def _send_data(self, data : str):
        if not self._websocket:
            raise Exception("Websocket is not initialized yet!")
        self._logger.log(10, "Send: " + data)
        self._websocket.send(data)

    @final
    def _reply_payload(self, _method : BridgeMethod, _id : Optional[int] = None, _result : Any = None, **extra):
        payload = BridgePayloadBaseModel(
            jsonrpc = "2.0",
            method = _method,
            params = extra,
            id = _id
        ).model_dump_json(exclude_none = True, by_alias = True)
        self._send_data(payload)
        return _id

    @final
    def _reply_result(self, _method : BridgeMethod, _result : Union[str, Dict[str, Any]], _id : Optional[int] = None):
        payload = BridgePayloadResultModel(
            jsonrpc = "2.0",
            method = _method,
            result = _result,
            id = _id
        ).model_dump_json(exclude_none = True, by_alias = True)
        self._send_data(payload)
        return _id

    @final
    def _connect(self, _type : str, _name : str, /):
        signal.signal(signal.SIGINT, self._stop_from_signal)
        signal.signal(signal.SIGTERM, self._stop_from_signal)
        self._logger.log(20, "Connecting with bridge")
        self._websocket_thread = Thread(target = self._pull_ws, args = (
            self.account.get_dev_server_websocket_url() + "?" + urlencode({
                "type": _type,
                "name": _name
            }), 
        ))
        self._websocket_thread.start()

# -----------------------------------------------------------
# Models
# -----------------------------------------------------------

class _Model(BaseModel):
    model_config = ConfigDict(
        extra = "ignore",
        frozen = False,
        validate_default = False,
        allow_inf_nan = False,
        alias_generator = to_camel
    )

class BridgeClientModel(_Model):
    client_id : Annotated[str, Field(alias = "clientId")]
    type : str
    name : str
    ip : str
    status : str
    itself : Annotated[bool, Field("own")]

class BridgePayloadBaseModel(_Model):
    jsonrpc : Literal["2.0"]
    method : BridgeMethod
    params : Annotated[Any, Field(None)]
    id : Optional[int] = None

class BridgePayloadErrorMessageModel(_Model):
    code : int
    message : str

class BridgePayloadErrorModel(_Model):
    jsonrpc : Literal["2.0"]
    error : BridgePayloadErrorMessageModel
    id : Optional[int] = None

class BridgePayloadResultModel(_Model):
    jsonrpc : Literal["2.0"]
    method : Optional[BridgeMethod] = None
    result : Union[str, Dict[str, Any]]
    id : Optional[int] = None

class BridgePayloadClientModel(BridgePayloadBaseModel):
    params : "BridgePayloadClientParamsModel"

class BridgePayloadClientParamsModel(_Model):
    event : Literal[BridgeEvent.CLIENT_CHANGE]
    clients : List[BridgeClientModel]

class BridgePayloadConnectModel(BridgePayloadBaseModel):
    params : "BridgePayloadConnectParamsModel"

class BridgePayloadConnectParamsModel(_Model):
    client_id : Annotated[str, Field(alias = "clientId")]

class BridgePayloadPackageModel(BridgePayloadBaseModel):
    params : "BridgePayloadPackageParamsModel"

class BridgePayloadPackageParamsModel(_Model):
    debugger : bool
    app_id : Annotated[int, Field(alias = "appid")]
    app_type : Annotated[Literal["app", "watchface"], Field(alias = "appType")]
    devices : List[int]
    name : str
    url : str
    updated_time : Annotated[int, Field(alias = "updatedTime")]
    preview_url : Annotated[str, Field(alias = "preview")]

class BridgePayloadSettingModel(BridgePayloadBaseModel):
    params : Union[
        "BridgePayloadSettingScreenshotParamsModel",
        "BridgePayloadSettingDeleteParamsModel"
    ]

class BridgePayloadSettingScreenshotParamsModel(_Model):
    type : Literal[BridgeEvent.SETTING_SCREENSHOT]

class BridgePayloadSettingDeleteParamsModel(_Model):
    type : Literal[BridgeEvent.SETTING_DELETE]
    app_id : Annotated[str, Field(alias = "appid")]

class BridgePayloadLogModel(BridgePayloadBaseModel):
    params : "BridgePayloadLogParamsModel"

class BridgePayloadLogParamsModel(_Model):
    message : str
    level : str = "DEBUG"
    time : int = 0

class BridgePayloadMessageModel(BridgePayloadBaseModel):
    params : "BridgePayloadMessageParamsModel"

class BridgePayloadMessageParamsModel(_Model):
    type : str
    title : str
    content : str
    time : int

def get_discriminator_value(v : Any):
    if isinstance(v, dict):
        if "error" in v:
            return "error"
        return v.get("method", None)
    if hasattr(v, "error"):
        return "error"
    return getattr(v, "method", None)

class BridgePayloadModel(RootModel):
    root : Annotated[
        Union[
            Annotated[BridgePayloadClientModel, Tag(BridgeMethod.CLIENT)],
            Annotated[BridgePayloadConnectModel, Tag(BridgeMethod.CONNECT)],
            Annotated[BridgePayloadPackageModel, Tag(BridgeMethod.PACKAGE)],
            Annotated[BridgePayloadSettingModel, Tag(BridgeMethod.SETTING)],
            Annotated[BridgePayloadResultModel, Tag(BridgeMethod.SCREENSHOT)],
            Annotated[BridgePayloadLogModel, Tag(BridgeMethod.LOG)],
            Annotated[BridgePayloadMessageModel, Tag(BridgeMethod.MESSAGE)],
            Annotated[BridgePayloadErrorModel, Tag("error")]
        ],
        Discriminator(get_discriminator_value)
    ]