#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from typing import Any, Dict, List, Literal, Optional, Self, Tuple, Annotated, Union

from zepplin.constants import DeviceShape, DeviceCapabilities, SystemVersion

import orjson
from pydantic import BaseModel, ConfigDict, Field, AliasPath, BeforeValidator, Json, model_validator

class _Model(BaseModel):
    model_config = ConfigDict(
        extra = "ignore",
        frozen = True,
        validate_default = False,
        allow_inf_nan = False
    )

def alias(path : str, **kwargs):
    return Field(validation_alias = AliasPath(*path.split(".")), **kwargs)

def convert_size(size : str):
    return str(size).replace("x", "*").split("*")

def convert_version(version : str):
    return version

def use_if_truthy(value):
    return value or None

def int_to_datetime(v):
    return datetime.strptime(str(v), "%Y%m%d%H%M")

# -------------------------------------------
# Models
# -------------------------------------------

class DeviceScreenModel(_Model):
    resolution : Annotated[Tuple[int, int], BeforeValidator(convert_size), alias("size")]
    preview : Annotated[Tuple[int, int], BeforeValidator(convert_size), alias("previewSize")]
    radius : Annotated[Optional[int], alias("rAngle")] = None

    def __repr__(self) -> str:
        return f"<Screen {self.resolution[0]}x{self.resolution[1]}>"

class DeviceNfcModel(_Model):
    tsm : str
    chip_type : Annotated[Optional[str], alias("chipType")] = None
    is_dynamic_city : Annotated[Optional[bool], alias("isDynamicCity")] = None

class DeviceModel(_Model):
    id : Annotated[int, alias("deviceSource")]
    name : Annotated[str, alias("productName")]
    codename : Annotated[str, alias("code")]
    created_at : Annotated[datetime, alias("createTime")]
    updated_at : Annotated[datetime, alias("updateTime")]
    shape : Annotated[DeviceShape, alias("value.shape")]
    os_name : Annotated[Literal["ZeppOS"], alias("value.os.name")]
    os_version : Annotated[SystemVersion, alias("value.os.version"), BeforeValidator(convert_version)]
    api_level : Annotated[SystemVersion, alias("value.os.apiLevel"), BeforeValidator(convert_version)]
    api_level_min : Annotated[SystemVersion, alias("value.os.apiLevelLimitMin"), BeforeValidator(convert_version)]
    api_level_max : Annotated[SystemVersion, alias("value.os.apiLevelLimitMax"), BeforeValidator(convert_version)]
    chip_type : Annotated[Literal["SOC", "MCU"], alias("value.chip.type")]
    chip_vendor : Annotated[str, alias("value.chip.manufacturer")]
    bluetooth_name : Annotated[str, alias("value.bluetooth")]
    device_type : Annotated[int, alias("value.deviceType")]
    product_name : Annotated[str, alias("value.productName")]
    product_name_en : Annotated[str, alias("value.productNameEN")]
    product_version : Annotated[int, alias("value.productVersion")]
    product_id : Annotated[int, alias("value.productId")]
    screen_size : Annotated[DeviceScreenModel, alias("value.screen")]
    nfc : Annotated[bool, alias("value.nfc")]
    nfc_extra : Annotated[Optional[DeviceNfcModel], alias("value.nfcExt"), BeforeValidator(use_if_truthy)] = None
    voice : Annotated[bool, alias("value.voice")]
    # These can be optional:
    released_at : Annotated[Optional[datetime], alias("value.releaseDate")] = None
    product_line : Annotated[Optional[str], alias("value.productLine")] = None
    product_serie : Annotated[Optional[str], alias("value.series")] = None
    capabilities : Annotated[Optional[List[DeviceCapabilities]], alias("value.capabilities")] = None
    thumbnail : Annotated[Optional[str], alias("value.thumbnail")] = None

class FirmwareModel(_Model):
    device_source : Annotated[int, alias("deviceSource")]
    production_source : Annotated[int, alias("productionSource")]
    languages : Annotated[List[str], BeforeValidator(lambda x: str(x).split(",")), alias("lang")]
    version : Annotated[str, alias("firmwareVersion")]
    flag : Annotated[int, alias("firmwareFlag")]
    url : Annotated[str, alias("firmwareUrl")]
    checksum_md5 : Annotated[str, alias("firmwareMd5")]
    checksum_crc : Annotated[int, alias("firmwareCrc32")]
    size : Annotated[int, alias("firmwareLength")]
    build_time : Annotated[datetime, alias("buildTime"), BeforeValidator(int_to_datetime)]
    support_8_bytes : Annotated[bool, alias("support8Bytes")]
    changelog : Annotated[str, alias("changeLog")]
    mirrors : Annotated[List[str], alias("downloadBackupPaths")]

class EmulatorImageVersionModel(_Model):
    user_limit : Annotated[int, alias("limit")]
    user_groups : Annotated[List[int], alias("visibleGroups"), Field(default_factory = lambda: [])]
    version_name : Annotated[str, alias("name")]
    version_code : Annotated[str, alias("code")]
    version_support : Annotated[str, alias("support")]
    changelog : Annotated[str, alias("log")]
    hidden : Annotated[bool, alias("hide")]
    id : Annotated[str, alias("id")]
    update_time : Annotated[datetime, alias("updatedTime")]
    size : Annotated[int, alias("fileSize")]
    url : Annotated[str, alias("filePath")]
    platform : Annotated[List[int], alias("platform"), Field(default_factory = lambda: [])]

class EmulatorImageModel(_Model):
    id : Annotated[str, alias("deviceId")]
    user_limit : Annotated[Optional[int], alias("limit")] = None
    codename : Annotated[Optional[str], alias("internal")] = None
    device_source : Annotated[List[int], alias("device")]
    image : Annotated[str, alias("deviceImage")]
    display_name : Annotated[str, alias("modelName")]
    screen_ratio : Annotated[Tuple[int, int], BeforeValidator(convert_size), alias("ratio")]
    releases : Annotated[List[EmulatorImageVersionModel], alias("released")]

class AccountInfoModel(_Model):
    nickname : Annotated[str, alias("nickName")]
    # Known values: "xiaomi" (if signed with Xiaomi account) or "huami" (if signed with Zepp account),
    # TODO: But allow other strings as a fallback anyway.
    source : Annotated[Union[str, Literal["xiaomi", "huami"]], alias("idSource")]
    create_time : Annotated[datetime, alias("createTime")]
    update_time : Annotated[datetime, alias("lastUpdateTime")]

class AccountDeveloperModel(_Model):
    user_id : Annotated[str, alias("userID")]
    user_name : Annotated[str, alias("userName")]
    avatar : Annotated[Optional[str], alias("avatar")]
    # Is empty anyway
    # mobile_phone : Annotated[str, alias("mobilePhone")]
    # mailbox : Annotated[Optional[str], alias("mailbox")]
    type : Annotated[int, alias("type")]
    create_time : Annotated[int, alias("createTime")]
    country : Annotated[str, alias("country")]
    bio : Annotated[str, alias("userIntroduction")]
    display_name : Annotated[str, alias("displayName")]

class AccountDetailsModel(_Model):
    nickname : Annotated[str, alias("nickName")]
    country_code : Annotated[str, alias("country_code")]
    country_state : Annotated[Optional[str], alias("countryState")] # Might be None for Xiaomi accounts
    region : Annotated[str, alias("region")]
    user_state : Annotated[str, alias("userState")]
    email : Annotated[Optional[str], alias("email")] # Might be None for Xiaomi accounts
    register_time : Annotated[datetime, alias("registerTimestamp")]
    country_time : Annotated[int, alias("countryConfirmedTime")]

class UserDeviceModel(_Model):
    device_type : Annotated[int, alias("deviceType")]
    device_source : Annotated[int, alias("deviceSource")]
    product_source : Annotated[Optional[int], alias("productVersion")]
    product_id : Annotated[Optional[int], alias("productId")]
    device_id : Annotated[str, alias("deviceId")]
    mac_address : Annotated[str, alias("macAddress")]
    serial : Annotated[str, alias("sn")]
    last_update : Annotated[datetime, alias("lastStatusUpdateTime")]
    last_active_update : Annotated[datetime, alias("lastActiveStatusUpdateTime")]
    active_status : Annotated[int, alias("activeStatus")]
    last_binded_platform : Annotated[str, alias("lastBindingPlatform")]
    firmware_version : Annotated[str, alias("firmwareVersion")]
    hardware_version : Annotated[str, alias("hardwareVersion")]
    display_name : Annotated[str, alias("displayName")]
    auth_key : Optional[str]
    extra : Annotated[Dict[str, Any], alias("additionalInfo")]

    @model_validator(mode = "wrap") # type: ignore
    def fill_additional_info(data : Dict[str, Any], handler) -> Self: # type: ignore
        new_data = data
        # Obtain additional information field, which is a JSON string,
        # so we load the JSON string and put the parsed JSON into the model.
        additional_info = orjson.loads(new_data.get("additionalInfo", None) or "{}")
        new_data["additionalInfo"] = additional_info
        # Some fields are found in additionalInfo object, so if the field
        # is empty in root data, try to read the one in the additional info.
        hw_version = new_data.get("hardwareVersion", None) or "0.0.0.0"
        if hw_version == "0.0.0.0":
            new_data["hardwareVersion"] = additional_info.get("hardwareVersion", None) or hw_version
        product_source = new_data.get("productVersion", None) or None
        if product_source is None:
            new_data["productVersion"] = additional_info.get("productVersion", None) or product_source
        product_id = new_data.get("productId", None) or None
        if product_id is None:
            new_data["productId"] = additional_info.get("productId", None) or product_id
        serial = new_data.get("sn", None) or None
        if serial is None:
            new_data["sn"] = additional_info.get("sn", None) or serial
        # Move the auth key to the root level.
        auth_key = additional_info.get("auth_key", None) or None
        new_data["auth_key"] = auth_key
        # Parse the rest of the fields with pydantic.
        return handler(data)

class StoreHomepageModel(_Model):
    total_count : Annotated[int, alias("total")]
    new_count : Annotated[int, alias("new_count")]
    new_items : Annotated[List["StorePartialItemModel"], alias("new")]
    categories : Annotated[List["StoreCategoryModel"], alias("categories")]
    # TODO: Missing "ranking", "subjects", "topics", "banner"

class StorePartialPublisherModel(_Model):
    description : Annotated[str, alias("description")]
    icon_url : Annotated[str, alias("icon")]
    id : Annotated[int, alias("id")]
    name : Annotated[str, alias("name")]

class StorePartialItemModel(_Model):
    auto_install : Annotated[bool, alias("auto_install")]
    customizable : Annotated[bool, alias("customizable")]
    device_support_version : Annotated[str, alias("device_support_version")]
    file_url : Annotated[str, alias("download_url")]
    store_id : Annotated[int, alias("id")]
    app_id : Annotated[int, alias("metas.builtin_id")]
    sku_id : Annotated[Optional[str], alias("sku_id")] = None # watchface-only
    thirdparty_id : Annotated[Optional[str], alias("thirdparty_product_id")] = None # watchface-only
    minimum_firmware_version : Annotated[Optional[str], alias("metas.minimum_firmware_version")] = None
    image_preview : Annotated[str, alias("image")]
    image_device : Annotated[str, alias("metas.device_image")]
    has_iap : Annotated[bool, alias("in_app_purchase")]
    is_purchased : Annotated[bool, alias("is_purchased")] = False
    is_free : Annotated[bool, alias("is_free")]
    name : Annotated[str, alias("name")]
    price_currency : Annotated[Optional[str], alias("price_currency")] = None
    price_value : Annotated[Optional[float], alias("price_value")] = None
    pay_channels : Annotated[Optional[List[str]], alias("pay_channels")] = None
    rank : Annotated[int, alias("rank")]
    size : Annotated[int, alias("size")]
    updated_at : Annotated[int, alias("updated_at")]
    version : Annotated[str, alias("version")]
    fw_size : Annotated[Optional[int], alias("fw_size")] = None # app-only
    checksum_md5 : Annotated[Optional[str], alias("metas.md5")] = None # app-only

class StorePartialItemWithPublisherModel(StorePartialItemModel):
    publisher : Annotated[StorePartialPublisherModel, alias("publisher")]
    title : Annotated[Optional[str], alias("brief_description")] = None # app-only

class StoreCategoryModel(_Model):
    name : Annotated[str, alias("category")]
    id : Annotated[int, alias("category_id")]
    current_page : Annotated[int, alias("page")]
    per_page : Annotated[int, alias("per_page")]
    total_page : Annotated[int, alias("total_page")]
    items : Annotated[List[StorePartialItemWithPublisherModel], alias("data")]

class StoreItemModel(StorePartialItemModel):
    description : Annotated[str, alias("description")]
    is_favorite : Annotated[bool, alias("favorited")]
    is_downloadable : Annotated[Optional[bool], alias("downloadable")] = None # app-only
    last_changelog : Annotated[Optional[str], alias("new_description")] = None # app-only
    preview_image : Annotated[List[str], alias("preview_pic")] = Field(default_factory = lambda: []) # app-only
    recommendations : Annotated[List[StorePartialItemModel], alias("recommend")] = Field(default_factory = lambda: [])
    app_config : Annotated[Optional[Json[Dict[str, Any]]], alias("config")] = None # app-only

class StoreItemChangelogVersionModel(_Model):
    description : Annotated[str, alias("description")]
    updated_at : Annotated[int, alias("updated_at")]
    version : Annotated[str, alias("version")]

class StoreItemChangelogModel(_Model):
    current_page : Annotated[int, alias("page")]
    per_page : Annotated[int, alias("per_page")]
    total_page : Annotated[int, alias("total_page")]
    items : Annotated[List[StoreItemChangelogVersionModel], alias("data")]

class DeviceFileInfoModel(_Model):
    file_type : Annotated[str, alias("fileType")]
    file_url : Annotated[str, alias("fileUrl")]
    file_url_previous : Annotated[str, alias("yesterdayFileUrl")]
    version : Annotated[int, alias("version")]
    request_type : Annotated[str, alias("requestType")]

class DeviceImageInfoModel(_Model):
    device_image : Annotated[str, alias("device_image")]
    preview_image : Annotated[str, alias("preview_image")]
    mask_image : Annotated[str, alias("screen_mask")]

# JSON response returned from watchface:// URIs
class WatchfaceURIResponseModel(_Model):
    app_id : Annotated[int, alias("appid")]
    name : str
    updated_at : int
    url : str
    preview_url : Annotated[str, alias("preview")]
    devices : List[int]