import signal
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread
from concurrent.futures import Future
from typing import Tuple
from urllib.parse import parse_qsl, urlencode, urlsplit
import webbrowser

from zepplin.constants import (
    LOGIN_HTTP_SERVER_ADDR, 
    LOGIN_HTTP_SERVER_CALLBACK_PATH,
    LOGIN_PROD_URL,
    LOGIN_XIAOMI_URL,
    LOGIN_XIAOMI_CALLBACK_URL,
    PLATFORM_APP,
    PROJECT_CLI_NAME,
    REGISTER_PROD_URL
)

import webview


def login_acquire_xiaomi_token() -> str:
    """
    Opens a webview to prompt signing in with a Xiaomi account, and blocks until
    the authorization has been completed. Returns the user token (as obtained in
    "https://hm.xiaomi.com/watch.do?code=..." callback).
    """

    # Construct a Xiaomi login URL, some details are documented here:
    # https://dev.mi.com/docs/passport/en/user-guide/
    auth_url = LOGIN_XIAOMI_URL + "?" + urlencode({
        "skip_confirm": "false",
        "client_id": "2882303761517383915", # Zepp App client ID
        "pt": "0",
        "scope": " ".join(["1", "6000", "16001", "20000"]),
        "redirect_uri": LOGIN_XIAOMI_CALLBACK_URL,
        "_locale": "en_US",
        "response_type": "code"
    })

    future = Future()

    def url_loaded(window : webview.Window):
        url = window.get_current_url() or ""
        if url == "about:blank":
            window.load_url(auth_url)
        elif url.startswith(LOGIN_XIAOMI_CALLBACK_URL):
            window.destroy()
            query = dict(parse_qsl(urlsplit(url).query))
            if "code" not in query:
                raise Exception("Couldn't obtain authorization code from URL: " + url)
            future.set_result(query["code"])

    window = webview.create_window("zepplin", url = "about:blank", width = 500, height = 700)
    window.events.loaded += url_loaded
    window.events.closed += lambda: None if future.done() else future.cancel()
    webview.start(debug = False)
    return future.result()


def login_acquire_zepp_token(register : bool = False) -> Tuple[str, str, str]:
    """
    Starts a local web server that listens for a HTTP request that contains required 
    query parameters for account credentials, so when redirected by a Zepp login page
    the path will contain the account parameters.
    
    Blocks until the HTTP request has been made, and returns 3-item tuples that contains
    user credentials; app token, user ID and CNAME list respectively.
    """

    serve_addr, serve_port = LOGIN_HTTP_SERVER_ADDR.split(":", maxsplit = 1)
    future = Future()

    def on_request(request : BaseHTTPRequestHandler):
        split = urlsplit(request.path)
        if split.path != LOGIN_HTTP_SERVER_CALLBACK_PATH:
            request.send_response(400)
            request.send_header("Content-Type", "text/plain; charset=UTF-8")
            request.end_headers()
            request.wfile.write(b"Invalid request.")
        else:
            query = dict(parse_qsl(split.query))
            # These query parameters must exist in callback URL.
            for key in ("apptoken", "userid", "cname",):
                if key not in query:
                    request.send_response(400)
                    request.send_header("Content-Type", "text/plain; charset=UTF-8")
                    request.end_headers()
                    request.wfile.write(b"Invalid authorization.")
                    return
            request.send_response(200)
            request.send_header("Content-Type", "text/plain; charset=UTF-8")
            request.end_headers()
            request.wfile.write(b"Done! You can now close this tab.")
            thr = Thread(target = request.server.shutdown)
            thr.start()
            future.set_result((
                query["apptoken"], query["userid"], query["cname"],
            ))
    
    handler = type("HTTPHandler", (BaseHTTPRequestHandler,), { 
        "do_GET": lambda self: on_request(self),
        "log_message": lambda self, *args, **kwargs: ...
    })
    httpd = HTTPServer((serve_addr, int(serve_port), ), handler, bind_and_activate = False)

    def serve_http():
        httpd.server_bind()
        httpd.server_activate()
        httpd.serve_forever()

    def stop_http(*args):
        httpd.shutdown()
        future.cancel()

    thread = Thread(target = serve_http)
    thread.start()
    signal.signal(signal.SIGINT, stop_http)

    # Make sure that callback host is "localhost", otherwise Zepp webpage
    # returns 404 error message in login page.
    callback_url = f"http://localhost:{serve_port}{LOGIN_HTTP_SERVER_CALLBACK_PATH}"
    auth_url = (LOGIN_PROD_URL if not register else REGISTER_PROD_URL) + "?" + urlencode({
        "project_name": PROJECT_CLI_NAME,
        "platform_app": PLATFORM_APP,
        "project_redirect_uri": callback_url
    })
    if not webbrowser.open_new(auth_url):
        print("Couldn't launch the browser automatically, open this URL in your browser:")
        print(auth_url)

    return future.result()