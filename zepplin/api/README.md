# `api`

This sub-package contains code to connect to Zepp endpoints and do account-related operations.

The most important classes and methods in this package:

* `zepplin.api.account.Account`
* `zepplin.api.bridge.BridgeBase`