#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from time import time
from typing import Literal, Optional, TYPE_CHECKING
from random import randint

from httpx import Request

from zepplin.constants import (
    API_BRIDGE_LIMIT,
    API_BRIDGE_RELAY,
    API_DEVICE_FILE,
    API_DEVICE_MASK, 
    API_DEVICE_ROM, 
    API_DEVICES, 
    API_HUAMI_LOGIN_PASSWORD, 
    API_HUAMI_LOGIN_TOKEN, 
    API_LOGOUT,
    API_MARKET_DEMO, 
    API_MARKET_HOME, 
    API_MARKET_ITEM_INFO,
    API_MARKET_ITEM_VERSIONS,
    API_MARKET_ITEMS,
    API_PACKAGE_UPLOAD, 
    API_USER, 
    API_USER_ADMIN,
    API_USER_CENTER,
    URL_DEVICES_BETA,
    URL_DEVICES_PROD,
    URL_EMULATOR_DOWNLOADS_PROD,
    URL_EMULATOR_DOWNLOADS_TEST, 
    STORE_TYPE_APP, 
    STORE_TYPE_WATCH, 
    USER_AGENT_AXIOS, 
    USER_AGENT_BROWSER,
    USER_AGENT_NODEFETCH, 
    USER_AGENT_ZEPP
)

if TYPE_CHECKING:
    from zepplin.api.account import AccountSession


class RequestCollection:
    @staticmethod
    def login_email_password(email : str, password : str):
        return Request(
            "POST", API_HUAMI_LOGIN_PASSWORD.format(
                email = email
            ), 
            data = {
                "client_id": "HuaMi",
                "country_code": "US",
                "json_response": "true",
                "name": email,
                "password": password,
                "redirect_uri": "https://s3-us-west-2.amazonaws.com/hm-registration/successsignin.html",
                "state": "REDIRECTION",
                "token": "access"
            }, 
            headers = RequestCollection.make_browser_headers()
        )

    @staticmethod
    def login_token_exchange(token : str, client : Literal["xiaomi", "amazfit", "huami"]):
        # random_mac = "02:00:00:{0:02X}:{1:02X}:{2:02X}".format(*[randint(0, 255) for _ in range(3)])
        return Request(
            "POST", API_HUAMI_LOGIN_TOKEN, 
            data = {
                "dn": ",".join([
                    "account.huami.com",
                    "api-user.huami.com",
                    "auth.huami.com",
                    "api-watch.huami.com",
                    "api-open.huami.com",
                    "api-mifit.huami.com"
                    # We don't need analytics domains.
                    # "app-analytics.huami.com",
                    # "api-analytics.huami.com",
                ]),
                "app_version": "5.9.2-play_100355" if client != "huami" else "4.3.0",
                "source": "com.huami.watch.hmwatchmanager" if client != "huami" else "",
                "country_code": "US",
                "device_id": "02:00:00:00:00:00",
                "third_name": \
                    "mi-watch" if client == "xiaomi" else 
                    "amazfit" if client == "amazfit" else \
                    "huami",
                "lang": "en",
                "device_model": "android_phone" if client != "huami" else "web",
                "allow_registration": "false",
                "app_name": "com.huami.midong" if client != "huami" else "com.huami.webapp",
                "code": token,
                "grant_type": "request_token" if client == "xiaomi" else "access_token"
            },
            headers = RequestCollection.make_browser_headers()
        )

    @staticmethod
    def user_info(account : "AccountSession"):
        return Request(
            "GET", API_USER.format(
                api_host = account.host_api,
                user_id = account.user_id
            ),
            headers = RequestCollection.make_axios_headers(account.app_token)
        )

    @staticmethod
    def user_info_developer(account : "AccountSession"):
        return Request(
            "GET", API_USER_CENTER.format(
                auth_host = account.host_auth
            ),
            headers = RequestCollection.make_console_headers(account.app_token)
        )

    @staticmethod
    def user_info_admin(account : "AccountSession"):
        return Request(
            "GET", API_USER_ADMIN.format(
                account_host = account.host_account,
                user_id = account.user_id
            ),
            headers = RequestCollection.make_browser_headers(account.app_token)
        )

    @staticmethod
    def user_devices(account : "AccountSession"):
        return Request(
            "GET", API_DEVICES.format(
                api_host = account.host_api,
                user_id = account.user_id
            ),
            params = RequestCollection.make_zepp_params(),
            headers = RequestCollection.make_app_headers(account.app_token)
        )

    @staticmethod
    def device_update(
        account : "AccountSession",
        product_id : int,
        device_id : int,
        hw_version : Optional[str] = None, 
        fw_version : Optional[str] = None,
        is_zepp : bool = True
    ):
        return Request(
            "GET", API_DEVICE_ROM.format(
                api_host = account.host_api
            ),
            params = {
                "productId": "1", # "103"
                "vendorSource": "1",
                "resourceVersion": "-1",
                "firmwareFlag": "-1",
                "vendorId": "1", # "343"
                "resourceFlag": "-1",
                "productionSource": product_id,
                "userid": account.user_id,
                "userId": account.user_id,
                "deviceSource": device_id,
                "fontVersion": "0",
                "diagnosticCode": "1",
                "fontFlag": "0",
                "appVersion": "8.7.1-play_151262" if is_zepp else "6.9.7_50764",
                "deviceType": "ALL",
                "firmwareVersion": fw_version or "0.0.0.0",
                "hardwareVersion": hw_version or "0.0.0.0",
                "sn": "00000/00000000000000",
                "support8Bytes": "true",
                **RequestCollection.make_zepp_params()
            },
            # "apptoken" is not required for this request.
            # headers = RequestCollection.make_app_headers(account.app_token, is_zepp)
            headers = RequestCollection.make_app_headers("0", is_zepp)
        )

    @staticmethod
    def device_file(account : "AccountSession", file_type : str):
        return Request(
            "GET", API_DEVICE_FILE.format(
                api_host = account.host_api,
                file = file_type
            ),
            headers = RequestCollection.make_app_headers(account.app_token)
        )

    @staticmethod
    def market_homepage(
        account : "AccountSession",
        device_id : int,
        is_watchface : bool
    ):
        return Request(
            "GET", API_MARKET_HOME.format(
                api_host = account.host_api,
                device_id = device_id,
                store_type = STORE_TYPE_WATCH if is_watchface else STORE_TYPE_APP
            ),
            headers = RequestCollection.make_app_headers(account.app_token),
            params = {
                "userid": account.user_id,
                "user_country": "US",
                "user_region": "",  #? A number ???
                "api_level": "100",
                "list_new_ids": "",
                "sn": "00000/00000000000000"
            }
        )

    @staticmethod
    def market_item_details(
        account : "AccountSession",
        device_id : int, 
        item_id : int,
        is_watchface : bool,
        recommendation_count : int = 4
    ):
        return Request(
            "GET", API_MARKET_ITEM_INFO.format(
                api_host = account.host_api,
                device_id = device_id,
                item_id = item_id,
                store_type = STORE_TYPE_WATCH if is_watchface else STORE_TYPE_APP
            ),
            headers = RequestCollection.make_app_headers(account.app_token),
            params = {
                "userid": account.user_id,
                "user_country": "US",
                "user_region": "", #? A number ???
                "api_level": "100",
                "id_type": "id",
                "firmware_version": "",
                "styles": "enable",
                "recommend_count": recommendation_count,
                "sn": "00000/00000000000000"
            }
        )

    @staticmethod
    def market_item_changelog(
        account : "AccountSession",
        device_id : int, 
        item_id : int
    ):
        return Request(
            "GET", API_MARKET_ITEM_VERSIONS.format(
                api_host = account.host_api,
                device_id = device_id,
                item_id = item_id,
                store_type = STORE_TYPE_APP
            ),
            headers = RequestCollection.make_app_headers(account.app_token),
            params = {
                "userid": account.user_id,
                "user_country": "US",
                "user_region": "", #? A number ???
                "api_level": "100",
                "sn": "00000/00000000000000"
            }
        )

    # TODO: Not sure what is that used for
    @staticmethod
    def market_demo(
        account : "AccountSession"
    ):
        return Request(
            "GET", API_MARKET_DEMO.format(
                api_host = account.host_api
            ),
            headers = RequestCollection.make_app_headers(account.app_token),
            params = {
                "userid": account.user_id,
                "user_country": "US",
                "user_region": "", #? A number ???
                "api_level": "100",
                "sn": "00000/00000000000000",
                "per_page": "20",
                "sort": "download",
                "customizable": "enable", # or "disable"
                "app_ids": "" # this is optional 
            }
        )

    @staticmethod
    def market_items(
        account : "AccountSession",
        device_id : int,
        is_watchface : bool
    ):
        return Request(
            "GET", API_MARKET_ITEMS.format(
                api_host = account.host_api,
                device_id = device_id,
                store_type = STORE_TYPE_WATCH if is_watchface else STORE_TYPE_APP
            ),
            headers = RequestCollection.make_app_headers(account.app_token),
            params = {
                "userid": account.user_id,
                "user_country": "US",
                "user_region": "", #? A number ???
                "api_level": "100",
                "customizable": "disable", #? also "enable" ???
                "app_ids": "", #? ???
                "versions": "", #? ???
                "page": "1",
                "per_page": "10",
                "sn": "00000/00000000000000"
            }
        )

    @staticmethod
    def bridge_relay(account : "AccountSession"):
        return Request(
            "GET", API_BRIDGE_RELAY.format(
                api_host = account.host_api
            ),
            headers = RequestCollection.make_axios_headers(account.app_token)
        )

    @staticmethod
    def bridge_limit(account : "AccountSession", group_id : int):
        return Request(
            "GET", API_BRIDGE_LIMIT.format(
                api_host = account.host_api,
                group_id = group_id
            ),
            params = {
                "userid": account.user_id
            },
            headers = RequestCollection.make_axios_headers(account.app_token)
        )

    @staticmethod
    def device_mask(account : "AccountSession", device_id : int):
        return Request(
            "GET", API_DEVICE_MASK.format(
                api_host = account.host_api,
                device_id = device_id
            ),
            params = {
                "userid": account.user_id
            },
            headers = RequestCollection.make_axios_headers(account.app_token)
        )

    # TODO: Implement package uploads
    @staticmethod
    def package_upload(
        account : "AccountSession", 
        package : bytes, 
        is_watchface : bool
    ):
        return Request(
            "POST", API_PACKAGE_UPLOAD.format(
                api_host = account.host_api
            ), 
            params = {
                "userid": account.user_id
            },
            files = {
                "file": package,
                "app_type": b"1" if is_watchface else b"2"
            },
            headers = RequestCollection.make_axios_headers(account.app_token)
        )

    @staticmethod
    def logout(account : "AccountSession"):
        return Request(
            "POST", API_LOGOUT.format(
                account_host = account.host_account
            ), 
            data = {
                "login_token": account.login_token
            },
            headers = RequestCollection.make_browser_headers()
        )

    @staticmethod
    def emulator_image_list(production : bool):
        current_time = str(int(time() * 1000))
        return Request(
            "GET", URL_EMULATOR_DOWNLOADS_PROD if production else URL_EMULATOR_DOWNLOADS_TEST,
            params = {
                "t": current_time
            },
            headers = RequestCollection.make_nodefetch_headers()
        )

    @staticmethod
    def device_list(production : bool):
        current_time = str(int(time() * 1000))
        return Request(
            "GET", URL_DEVICES_PROD if production else URL_DEVICES_BETA,
            params = {
                "no-cache": str(current_time)
            },
            headers = {
                **RequestCollection.make_axios_headers(),
                "Cache-Control": "no-cache"
            }
        )

    @staticmethod
    def make_zepp_params():
        return {
            "appid": "2882303761517383915",
            "channel": "play",
            "enableMultiDevice": "true",
            "device_type": "android_phone",
            "cv": "151262_8.7.1-play",
            "device": "android_33",
            "v": "2.0",
            "lang": "en_US",
            "country": "US"
        }

    @staticmethod
    def make_app_headers(app_token : str, is_zepp : bool = True):
        return {
            "User-Agent": USER_AGENT_ZEPP,
            "apptoken": app_token,
            "channel": "play",
            "appname": "com.huami.midong" if is_zepp else "com.xiaomi.hm.health",
            "appplatform": "android_phone",
            "cv": "151262_8.7.1-play",
            "v": "2.0",
            "lang": "en_US",
            "country": "US",
            "hm-privacy-diagnostics": "false",
            "hm-privacy-ceip": "true"
        }

    @staticmethod
    def make_browser_headers(app_token : Optional[str] = None):
        return {
            "User-Agent": USER_AGENT_BROWSER,
            "Origin": "https://user.zepp.com",
            "Referer": "https://user.zepp.com/",
            "app_name": "com.huami.webapp",
            **({} if not app_token else {"Authorization": f"Bearer {app_token}"})
        }

    @staticmethod
    def make_console_headers(app_token : str):
        return {
            "User-Agent": USER_AGENT_BROWSER,
            "Origin": "https://console.zepp.com",
            "Referer": "https://console.zepp.com/",
            "apptoken": app_token
        }

    @staticmethod
    def make_axios_headers(app_token : Optional[str] = None):
        return {
            "User-Agent": USER_AGENT_AXIOS,
            **({} if not app_token else {"apptoken": app_token})
            # Simulator creates these headers:
            # "app_token": app_token,
            # "user_id": user_id,
            # "app_name": PLATFORM_SIMULATOR
        }

    @staticmethod
    def make_nodefetch_headers():
        return {
            "User-Agent": USER_AGENT_NODEFETCH,
            "Content-Type": "application/octet-stream"
        }