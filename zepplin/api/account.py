#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from base64 import b64decode
from dataclasses import dataclass
from pathlib import Path
from typing import List, Literal, Optional
from time import time
from functools import lru_cache
from urllib.parse import urlsplit, urlunsplit

from zepplin.api.models import (
    AccountDetailsModel, 
    AccountInfoModel,
    AccountDeveloperModel,
    DeviceFileInfoModel, 
    DeviceImageInfoModel,
    DeviceModel,
    EmulatorImageModel, 
    FirmwareModel, 
    StoreHomepageModel, 
    StoreItemChangelogModel, 
    StoreItemModel, 
    StorePartialItemModel, 
    UserDeviceModel,
    WatchfaceURIResponseModel
)
from zepplin.constants import (
    DEVICES_LIST_BETA_DECRYPT_KEY,
    GROUP_ID_1_PROD,
    GROUP_ID_2_PROD,
    AUTH_TOKEN_EXPIRE_TIME,
    DEFAULT_CNAMES
)
from zepplin.api.requests import RequestCollection
from zepplin.api.helpers import login_acquire_xiaomi_token, login_acquire_zepp_token

import httpx
import orjson
from nacl.secret import SecretBox

from zepplin.logging import get_logger

@dataclass(frozen = True, slots = True)
class AccountSession:
    login_token : Optional[str]
    app_token : str
    user_id : str
    servers : str
    expiration : Optional[int]

    @property
    def host_api(self) -> str:
        return next((x for x in self.servers.split(",") if "api-mifit" in x))

    @property
    def host_auth(self) -> str:
        return next((x for x in self.servers.split(",") if "auth-" in x))

    @property
    def host_account(self) -> str:
        return next((x for x in self.servers.split(",") if "account-" in x))

    @property
    def is_expired(self) -> bool:
        if self.expiration is not None:
            return int(time() * 1000) >= self.expiration
        return False

    @classmethod
    def ephemeral(cls, app_token : str, user_id : str):
        """
        Returns a new AccountSession that uses default CNAMEs and has no expiration
        set with only app token and user ID provided. It may work or not.
        """
        return cls(
            login_token = None,
            app_token = app_token,
            user_id = user_id,
            servers = ",".join(DEFAULT_CNAMES),
            expiration = None
        )

    def to_dict(self):
        return { 
            "login_token": self.login_token,
            "app_token": self.app_token,
            "user_id": self.user_id,
            "servers": self.servers,
            "expiration": self.expiration
        }

class Account:
    """
    Login with Zepp account and call their endpoints.
    """
    __slots__ = (
        "session",
        "_client",
        "_logger"
    )

    def __init__(self, session : AccountSession) -> None:
        self.session = session
        self._client = httpx.Client()
        self._logger = get_logger("api")
        def log_request(req : httpx.Request):
            self._logger.log(10, f"HTTP -> {req.method}, {req.url}, headers: {str(dict(req.headers))}")
        def log_response(resp : httpx.Response):
            self._logger.log(10, f"HTTP <- {resp.status_code}, {resp.url}, headers: {str(dict(resp.headers))}")
        self._client.event_hooks["request"] = [log_request]
        self._client.event_hooks["response"] = [log_response]

    @classmethod
    def from_file(cls, file : Path):
        """
        Returns a new Account object with credentials from given file.
        """
        if not file.is_file():
            raise ValueError("Given path is not a file.")
        return cls(AccountSession(**orjson.loads(file.read_bytes())))

    def to_file(self, file : Path):
        """
        Save the current credentials to a file.
        """
        with file.open("wb") as fs:
            fs.write(orjson.dumps(
                self.session.to_dict(), 
                option = orjson.OPT_INDENT_2
            ))

    @classmethod
    def login_flow(cls, register : bool = False):
        """
        Opens Zepp login page in default browser, and launches a local HTTP server that
        listens for the callback URL. Blocks until the authorization has been completed.

        Returns a new Account object with obtained credentials.
        """
        app_token, user_id, cname = login_acquire_zepp_token(register)
        return cls(AccountSession(
            login_token = None,
            app_token = app_token,
            user_id = user_id,
            servers = cname,
            expiration = int(time() * 1000) + AUTH_TOKEN_EXPIRE_TIME
        ))

    @classmethod
    def login_xiaomi(cls):
        """
        Opens a webview to prompt signing in with a Xiaomi account, and blocks until
        the authorization has been completed.
        
        Returns a new Account object with obtained credentials.
        """
        access_code = login_acquire_xiaomi_token()
        return cls(cls.exchange_login_token(
            token = access_code, 
            client_type = "xiaomi"
        ))

    @classmethod
    def login_password(cls, email : str, password : str):
        """
        Login with a Zepp account by specifying e-mail and password.
        Returns a new Account object with obtained credentials.
        """
        request = RequestCollection.login_email_password(email, password)
        with httpx.Client() as client:
            resp = client.send(request)
            resp.raise_for_status()
            output = resp.json()
            if not output.get("ok", None):
                raise Exception("Couldn't sign you in, try again?")
            return cls(cls.exchange_login_token(
                token = output["access"], 
                client_type = "huami"
            ))

    @classmethod
    def create_foreign_session(cls, app_token : str):
        """
        Create an account session from by providing an already signed-in session's
        app token only, obtaining the user ID from Zepp servers. It should not be 
        preferred for account login.
        """
        partial_session = AccountSession.ephemeral(app_token, "")
        request = RequestCollection.user_info_developer(partial_session)
        with httpx.Client() as client:
            resp = client.send(request)
            resp.raise_for_status()
            partial = AccountDeveloperModel.model_validate_json(resp.content)
            return AccountSession(
                login_token = partial_session.login_token,
                app_token = partial_session.app_token,
                user_id = partial.user_id,
                servers = partial_session.servers,
                expiration = partial_session.expiration
            )

    @staticmethod
    def exchange_login_token(token : str, *, client_type : Literal["xiaomi", "amazfit", "huami"]) -> AccountSession:
        """
        Sends a request to exchange a refresh or access token to Zepp credentials.
        Returns a AccountSession object.
        """
        request = RequestCollection.login_token_exchange(token, client_type)
        with httpx.Client() as client:
            resp = client.send(request)
            resp.raise_for_status()
            output = resp.json()
            if output.get("result", None) != "ok":
                raise Exception("Couldn't sign you in, token might be expired, try again")
            return AccountSession(
                login_token = output["token_info"]["login_token"],
                app_token = output["token_info"]["app_token"],
                user_id = output["token_info"]["user_id"],
                servers = ",".join([x for domains in output["domains"] for x in domains["cnames"]]),
                # Assuming "app_ttl" is in seconds.
                expiration = int(time() * 1000) + (output["token_info"]["app_ttl"] * 1000)
            )

    def get_user_devices(self) -> List[UserDeviceModel]:
        """
        Requests the list of devices that linked to current account, such as their MAC addresses,
        names and auth keys. Returns a list of UserDeviceModel objects.

        Note that even if you have linked devices on your account, and signed in with login_flow()
        method, server may return an empty list. To make your devices appear in the returned list,
        make sure you have signed in with your e-mail and password.
        """
        self.raise_if_expired()
        request = RequestCollection.user_devices(self.session)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        if "items" not in output:
            raise Exception("Couldn't fetch devices list")
        devices = []
        for item in output["items"]:
            devices.append(UserDeviceModel.model_validate(item))
        return devices

    def get_user_basic_info(self) -> AccountInfoModel:
        """
        Requests the basic user information for currently given credentials.
        Returns an AccountInfoModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.user_info(self.session)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return AccountInfoModel.model_validate(output)

    def get_user_admin_info(self) -> AccountDetailsModel:
        """
        Requests the detailed user information (such as e-mail address) for 
        currently given credentials. Returns an AccountDetailsModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.user_info_admin(self.session)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return AccountDetailsModel.model_validate(output)

    def get_device_update(
        self,
        device_source : int,
        product_source : int,
        hw_version : Optional[str] = None, 
        fw_version : Optional[str] = None,
        is_xiaomi : bool = False
    ):
        """
        Requests the new firmware updates for given device code/ID. You can obtain your device's
        related information with get_user_devices().
        """
        self.raise_if_expired()
        request = RequestCollection.device_update(
            self.session, product_source, device_source, hw_version, fw_version,
            is_zepp = not is_xiaomi
        )
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        if "firmwareVersion" not in output:
            raise Exception("Couldn't get latest firmware information.")
        return FirmwareModel.model_validate(output)

    def get_market_homepage(
        self, device_source : int, is_watchface : bool = False
    ) -> StoreHomepageModel:
        """
        Get the homepage of the Zepp app store.
        Returns a StoreHomepageModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.market_homepage(self.session, device_source, is_watchface)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return StoreHomepageModel.model_validate(output)

    def get_market_item_details(
        self, device_source : int, item_id : int, is_watchface : bool = False
    ) -> StoreItemModel:
        """
        Get the details for a specific item ID in the Zepp app store.
        Returns a StoreItemModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.market_item_details(self.session, device_source, item_id, is_watchface)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return StoreItemModel.model_validate(output)

    def get_market_item_changelog(
        self, device_source : int, item_id : int
    ) -> StoreItemChangelogModel:
        """
        Get the changelog for a specific item ID in the Zepp app store. Note that only mini-apps
        (so not watchfaces) can have a changelog. For watchfaces, this method will fail.
        Returns a StoreItemChangelogModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.market_item_changelog(self.session, device_source, item_id)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return StoreItemChangelogModel.model_validate(output)

    def get_market_items(
        self, device_source : int, is_watchface : bool = False
    ) -> List[StorePartialItemModel]:
        """
        Get the list of items in the Zepp app store.
        Returns a list of StorePartialItemModel objects.
        """
        self.raise_if_expired()
        request = RequestCollection.market_items(self.session, device_source, is_watchface)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        items = []
        for i in output["data"]:
            items.append(StorePartialItemModel.model_validate(i))
        return items

    def get_device_file(
        self, file : Literal["AGPS_ALM", "AGPSZIP", "LLE", "AGPS", "EPO", "VOICE"]
    ) -> List[DeviceFileInfoModel]:
        """
        Requests a file for the device, such as AGPS.
        Returns a list of DeviceFileInfoModel objects.
        """
        self.raise_if_expired()
        request = RequestCollection.device_file(self.session, file)
        resp = self._client.send(request)
        resp.raise_for_status()
        files = []
        for file in resp.json():
            files.append(DeviceFileInfoModel.model_validate(file))
        return files

    def logout(self) -> bool:
        """
        Sign out from current account. Returns True if logout is successful, otherwise False.
        
        Signing out from an account is only possible if credentials contains a login token, 
        otherwise this method will just return False in that case.
        """
        if not self.session.login_token:
            return False
        request = RequestCollection.logout(self.session)
        resp = self._client.send(request)
        resp.raise_for_status()
        return resp.json().get("result", None) == "ok"

    # ./build/electron/process/create-webview.js:checkLimit()
    def get_limits(self):
        """
        Returns the limits. It appears to be only used in fetching the
        emulator firmware list to filter out the firmwares that is not
        in this account limit value, but since we don't do that here, this 
        method has no purpose for us apparently.
        """
        self.raise_if_expired()
        limit = 1
        for group_id in (GROUP_ID_1_PROD, GROUP_ID_2_PROD, ):
            request = RequestCollection.bridge_limit(self.session, group_id)
            resp = self._client.send(request)
            if resp.is_success:
                if resp.json()["is_in_group"]:
                    if group_id == GROUP_ID_1_PROD:
                        limit = 2
                    if group_id == GROUP_ID_2_PROD:
                        limit = 3
        return limit

    # ./build/electron/modules/screenshot/screenshot.model.js:getMaskImage()
    def get_device_image(self, device_source : int) -> DeviceImageInfoModel:
        """
        Get screen frame image (mask) and product image for given device ID.
        Returns a DeviceImageInfoModel object.
        """
        self.raise_if_expired()
        request = RequestCollection.device_mask(self.session, device_source)
        resp = self._client.send(request)
        resp.raise_for_status()
        output = resp.json()
        return DeviceImageInfoModel.model_validate(output)

    # - ./build/electron/process/init-eimulator-list.js
    #
    # Simulator app stores the devices in this format:
    # { name: '小米手环 7', device: [260, 261, 262, 263, 264, 265, 266], }
    @lru_cache
    @staticmethod
    def get_emulator_list(production : bool = True) -> List[EmulatorImageModel]:
        request = RequestCollection.emulator_image_list(production)
        with httpx.Client() as client:
            resp = client.send(request)
            resp.raise_for_status()
            output = resp.json()
            result = []
            for i in output:
                # Non-production endpoint returns a dummy value, which doesn't represent 
                # a valid emulator and rather contain missing keys, so skip it without 
                # even trying to validate it. (it would fail otherwise)
                if not production:
                    if i.get("deviceId", None) == "b2ceffba45daebcfb8ab9cf4302fe381":
                        continue
                result.append(EmulatorImageModel.model_validate(i))
            return result

    @lru_cache
    @staticmethod
    def get_device_list(production : bool = True) -> List[DeviceModel]:
        NONCE_LENGTH = 24
        cipher = None
        if not production:
            cipher = SecretBox(bytes.fromhex(DEVICES_LIST_BETA_DECRYPT_KEY))
        request = RequestCollection.device_list(production)
        with httpx.Client() as client:
            resp = client.send(request)
            resp.raise_for_status()
            output = resp.json()
            result = []
            for i in output:
                element = i
                # Listing beta devices returns an encrypted 
                # value for each device, so we decrypt them.
                if cipher:
                    decoded = b64decode(element)
                    nonce, message = decoded[:NONCE_LENGTH], decoded[NONCE_LENGTH:]
                    element = cipher.decrypt(message, nonce)
                    result.append(DeviceModel.model_validate_json(element))
                else:
                    result.append(DeviceModel.model_validate(element))
            return result

    def get_dev_server_websocket_url(self) -> str:
        """
        Requests a new websocket URL for connecting to Developer Bridge. Not meant to be called directly,
        see `EmulatorBridge` class which automatically handles the connection.
        """
        self.raise_if_expired()
        request = RequestCollection.bridge_relay(self.session)
        resp = self._client.send(request)
        resp.raise_for_status()
        return resp.json()["url"]

    def _receive_scan_uri(self, uri : str, device_source : Optional[int] = None):
        result = urlsplit(uri)
        if result.scheme == "watchface":
            new_url = urlunsplit(result._replace(scheme = "https"))
            resp = self._client.get(
                new_url, 
                params = None if device_source is None else {"deviceSource": device_source},
                headers = {"apptoken": self.session.app_token}
            )
            resp.raise_for_status()
            model = WatchfaceURIResponseModel.model_validate_json(resp.content)
            return model

    @property
    def is_expired(self) -> bool:
        """
        Returns True if credentials has expired. This doesn't mean that credentials are still 
        valid, it still might be invalided due to various reasons.
        """
        return self.session.is_expired

    def raise_if_expired(self):
        """
        Raises an exception if credentials are expired.
        """
        if self.is_expired:
            raise Exception("Account credentials has expired, please login again.")