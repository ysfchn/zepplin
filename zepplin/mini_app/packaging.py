#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from threading import Thread
from struct import pack, unpack
from typing import IO, List, Literal, Optional, Tuple, cast
from http.server import BaseHTTPRequestHandler, HTTPServer
import socket
from urllib.parse import urlsplit
from pathlib import Path

from zepplin.constants import ZEPPLIN_WEB_SERVER_PORT
from zepplin.utils import chunked_read

from PIL import Image
from qrcode.main import QRCode

def export_to_tga(
    image : Image.Image,
    tga_format : Literal["TGA-P", "TGA-16", "TGA-32", "TGA-RLP"] = "TGA-P",
    is_nxp : bool = False
):
    """
    Convert existing Image object to TGA format and return the bytes of the converted TGA image.
    Image formats:
    
    - TGA-P: Color-mapped (palette based) image.
    - TGA-RLP: Length encoded, color-mapped image. (uses per-line pixel compression)
    - TGA-16: 16-bit RGB image, with no alpha.
    - TGA-32: 32-bit RGBA image, but comes with large size.
    """
    # Portions of this function (export_to_tga()) has adopted from zmake's code, which is licensed under GPLv3.
    # https://github.com/melianmiko/zmake/blob/d668c02c32987ab995961af58d6e1a52ce8d3922/zmake/tga_save.py
    if image.mode != "RGBA":
        raise ValueError("Image mode must be RGBA!")
    output = bytearray()
    # Make sure that the image width is a multiple of 16 for NXP.
    original_width = image.width
    if is_nxp and (original_width % 16 != 0):
        tga_width = original_width + 16 - (original_width % 16)
        assert tga_width % 16 == 0
        new_img = Image.new(image.mode, (tga_width, image.height))
        new_img.paste(image)
        image = new_img
    # List of colors
    colors : List[Tuple[int, int, int, int]] = []
    if tga_format in ["TGA-P", "TGA-RLP"]:
        colors_list = cast(Optional[List[Tuple[int, Tuple[int, int, int, int]]]], image.getcolors(maxcolors = 256))
        if not colors_list:
            raise ValueError("There are more than 256 colors in this image, make sure to convert it beforehand!")
        for _, color in colors_list:
            colors.append(color)
        # Make sure that there are exactly 256 colors.
        colors.extend([(0, 0, 0, 255)] * (256 - len(colors)))
    # Refer here for TGA specification:
    # https://www.martinreddy.net/gfx/2d/TGA.txt
    byte_format = "<BBBxxHBxxxxHHBB4sH" + ("x" * 40)
    header = pack(byte_format,
        46,
        1 if tga_format in ["TGA-P", "TGA-RLP"] else 0,
        1 if tga_format == "TGA-P" else 9 if tga_format == "TGA-RLP" else 2,
        len(colors), 
        32,
        image.width, 
        image.height,
        16 if tga_format == "TGA-16" else \
        32 if tga_format == "TGA-32" else 8, 
        32, b"SOMH",
        original_width
    )
    output.extend(header)
    if colors:
        for r, g, b, a in colors:
            output.extend([r if is_nxp else b, g, b if is_nxp else r, a])
    # TGA-32
    if tga_format == "TGA-32":
        for r, g, b, a in cast(List[Tuple[int, int, int, int]], image.getdata()):
            output.extend([r if is_nxp else b, g, b if is_nxp else r, a])
    # TGA-16
    elif tga_format == "TGA-16":
        for r, g, b, _ in cast(List[Tuple[int, int, int, int]], image.getdata()):
            rr = round(31 / 255 * r)
            gg = round(63 / 255 * g)
            bb = round(31 / 255 * b)
            if is_nxp:
                rr, bb = bb, rr
            output.append(((gg & 0b111) << 5) + bb)
            output.append((rr << 3) + (gg >> 3))
    # TGA-P
    elif tga_format == "TGA-P":
        for pixel in cast(List[Tuple[int, int, int, int]], image.getdata()):
            index = colors.index(pixel)
            output.append(index)
    # TGA-RLP
    elif tga_format == "TGA-RLP":
        out = bytearray([0])
        head_index = 0
        for pixel in cast(List[Tuple[int, int, int, int]], image.getdata()):
            index = colors.index(pixel)
            head = out[head_index]
            if len(out) == 1:
                out.append(index)
            elif head & 128 and index == out[-1] and head < 255:
                out[head_index] += 1 # Existing RL
            elif out[-1] == index and head_index != len(out) - 1:
                if head == 0: # New RL
                    out[head_index] += 128 + 1 # Empty RGB
                else:
                    out[head_index] -= 1
                    if head_index == len(out) - 2:
                        out.append(index)
                    head_index = len(out) - 1
                    out[head_index] = 128 + 1
                    out.append(index)
            elif head < 127:
                # Existing RGB
                out.append(index)
                out[head_index] += 1
            else:
                # New RGB
                out.append(0)
                out.append(index)
                head_index = len(out) - 2
        output.extend(out)
    return bytes(output)


def import_from_tga(
    buffer : IO[bytes],
    is_nxp : bool = False
):
    """
    Read existing TGA-encoded image from given readable file-like object and return an Image object.
    It should return the same Image (visually) that used in export_to_tga().
    """
    # Portions of this function (import_from_tga()) has adopted from zmake's code, which is licensed under GPLv3.
    # https://github.com/melianmiko/zmake/blob/d668c02c32987ab995961af58d6e1a52ce8d3922/zmake/tga_load.py
    byte_format = "<BBBxxHBxxxxHHBB4sH" + ("x" * 40)
    header = unpack(byte_format, buffer.read(64))
    width, height = header[5], header[6],
    # TGA-32
    if (header[1] == 0) and (header[2] == 2) and (header[7] == 32):
        pixels = []
        for _ in range(height * width):
            if is_nxp:
                r, g, b, a = buffer.read(4)
            else:
                b, g, r, a = buffer.read(4)
            pixels.append((r, g, b, a, ))
        image = Image.new("RGBA", (width, height))
        image.putdata(pixels)
    # TGA-16
    elif (header[1] == 0) and (header[2] == 2) and (header[7] == 16):
        pixels = []
        for _ in range(height * width):
            b2, b1 = buffer.read(2)
            v = (b1 << 8) + b2
            r = (v & 0b1111100000000000) >> 11
            g = (v & 0b0000011111100000) >> 5
            b = v & 0b0000000000011111
            if is_nxp:
                r, b = b, r
            pixels.append((int(r * 255/31), int(g * 255/63), int(b * 255/31), 255, ))
        image = Image.new("RGBA", (width, height))
        image.putdata(pixels)
    # TGA-P and TGA-RLP
    elif (header[1] == 1) and (header[2] in [1, 9]):
        palette = bytearray()
        image_data = bytearray()
        for _ in range(header[3]):
            if is_nxp:
                r, g, b, a = buffer.read(4)
            else:
                b, g, r, a = buffer.read(4)
            palette.extend([r, g, b, a])
        # TGA-RLP
        if header[2] == 9:
            while len(image_data) < width * height:
                head = buffer.read(1)[0]
                count = (head & 127) + 1
                if head & 128:
                    index = buffer.read(1)[0]
                    for _ in range(count):
                        image_data.append(index)
                else:
                    for _ in range(count):
                        val = buffer.read(1)[0]
                        image_data.append(val)
        # TGA-P
        else:
            image_data.extend(buffer.read(width * height))
        image = Image.new("P", (width, height))
        image.putpalette(palette, "RGBA")
        image.putdata(image_data)
    else:
        raise ValueError("The input file is not a TGA image!")
    # Use original_width if available.
    if (header[9] == b"SOMH") and (header[10]) and (image.width != header[10]):
        image = image.crop((0, 0, header[10], image.height))
    return image.convert("RGBA")


def get_device_ip():
    result = None
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0)
    try:
        sock.connect(("10.254.254.254", 1))
        result = sock.getsockname()[0]
    except Exception:
        pass
    finally:
        sock.close()
    return result or "127.0.0.1"


def start_file_local_server(file : Path):
    result = None
    def on_request(request : BaseHTTPRequestHandler):
        nonlocal result
        split = urlsplit(request.path)
        if split.path == "/file":
            request.send_response(200)
            request.send_header("Content-Type", "application/octet-stream")
            request.send_header("Content-Disposition", "attachment; filename=\"fw.bin\"")
            request.end_headers()
            opened = file.open("rb")
            for i in chunked_read(opened, 1024):
                request.wfile.write(i)
            opened.close()
            thr = Thread(target = request.server.shutdown)
            thr.start()
        else:
            request.send_response(404)
            request.send_header("Content-Type", "text/plain; charset=UTF-8")
            request.end_headers()
            request.wfile.write(b"Not found")
    def serve_http():
        handler = type("HTTPHandler", (BaseHTTPRequestHandler,), { "do_GET": lambda self: on_request(self) })
        httpd = HTTPServer(("0.0.0.0", ZEPPLIN_WEB_SERVER_PORT), handler, bind_and_activate = False)
        httpd.server_bind()
        httpd.server_activate()
        httpd.serve_forever()
    thread = Thread(target = serve_http)
    thread.start()
    # Display a QR code on the screen with a URL pointing to this web server,
    # so it can be scanned from a QR reader on the phone.
    qr = QRCode(version = 1)
    qr.add_data(f"http://{get_device_ip()}:{ZEPPLIN_WEB_SERVER_PORT}/file")
    qr.make_image().show() # type: ignore
    thread.join()
    return result