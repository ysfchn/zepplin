#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

# Reference:
# https://github.com/zepp-health/zeppos-samples/blob/main/application/1.0/todo-list/shared/message.js
#
# See also:
# https://docs.zepp.com/docs/1.0/guides/best-practice/bluetooth-communication/ 

from enum import IntEnum, StrEnum
from struct import unpack, pack
from typing import NamedTuple

MESSAGE_SIZE = 3600
MESSAGE_HEADER = 16
MESSAGE_PAYLOAD = MESSAGE_SIZE - MESSAGE_HEADER
MESSAGE_PROTO_HEADER = 66
MESSAGE_PROTO_PAYLOAD = MESSAGE_PAYLOAD - MESSAGE_PROTO_HEADER

class BluetoothMessageFlag(IntEnum):
    RUNTIME = 0
    APP = 1

class BluetoothMessageType(IntEnum):
    SHAKE = 1
    CLOSE = 2
    HEART = 3
    DATA = 4
    DATA_WITH_SYSTEM_TOOL = 5
    LOG = 6

class BluetoothMessageRuntimeType(IntEnum):
    INVOKE = 1

class BluetoothMessageVersion(IntEnum):
    VERSION_1 = 1

class BluetoothMessagePayloadType(StrEnum):
    EMPTY = "empty"
    JSON = "json"
    TEXT = "text"
    BINARY = "bin"

class BluetoothMessagePayloadTypeOp(IntEnum):
    EMPTY = 0
    JSON = 1
    TEXT = 2
    BINARY = 3

class BluetoothMessagePayloadOp(IntEnum):
    CONTINUED = 0
    FINISHED = 1

class BluetoothMessageData(NamedTuple):
    # Header, size = 16
    flag : BluetoothMessageFlag
    version : BluetoothMessageVersion
    type : BluetoothMessageType
    port1 : int
    port2 : int
    app_id : int
    extra : int
    # Arbitrary data
    payload : bytes

class BluetoothMessagePayloadData(NamedTuple):
    trace_id : int
    parent_id : int
    span_id : int
    seq_id : int
    total_length : int
    payload_length : int
    payload_type : int
    op_code : int
    timestamp1 : int
    timestamp2 : int
    timestamp3 : int
    timestamp4 : int
    timestamp5 : int
    timestamp6 : int
    timestamp7 : int
    content_type : int # BluetoothMessagePayloadTypeOp
    data_type : int
    extra1 : int
    extra2 : int
    extra3 : int
    data : bytes

def parse_message(data : bytes):
    header = unpack("<BBHHHII", data[:MESSAGE_HEADER])
    return BluetoothMessageData(
        flag = BluetoothMessageFlag(header[0]),
        version = BluetoothMessageVersion(header[1]),
        type = BluetoothMessageType(header[2]),
        port1 = header[3],
        port2 = header[4],
        app_id = header[5],
        extra = header[6],
        payload = data[MESSAGE_HEADER:]
    )

# TODO: unused
def parse_message_payload(payload : bytes):
    header = unpack("<IIIIIIBBIIIIIIIBBHII", payload[:MESSAGE_PROTO_HEADER])
    return BluetoothMessagePayloadData(
        trace_id = header[0],
        parent_id = header[1],
        span_id = header[2],
        seq_id = header[3],
        total_length = header[4],
        payload_length = header[5],
        payload_type = header[6],
        op_code = header[7],
        timestamp1 = header[8],
        timestamp2 = header[9],
        timestamp3 = header[10],
        timestamp4 = header[11],
        timestamp5 = header[12],
        timestamp6 = header[13],
        timestamp7 = header[14],
        content_type = header[15],
        data_type = header[16],
        extra1 = header[17],
        extra2 = header[18],
        extra3 = header[19],
        data = payload[MESSAGE_PROTO_PAYLOAD:]
    )

def dump_message(message : BluetoothMessageData):
    # We get the "payload" or "data" field of the message by [:-1].
    return pack("<BBHHHII", *message[:-1]) + message[-1]