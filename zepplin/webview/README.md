# `webview`

This sub-package contains functionality to running Side Service and Settings UI APIs, which is compatible with existing Zepp OS mini-apps. For actual Side Service and Settings UI JavaScript APIs implementation that mini-apps can call, see [`zepplin_web`](../../zepplin_web/).

The most important classes and methods in this package:

* `zepplin.webview.classes.Webview`

---

Devices equipped with Zepp OS doesn't have an internet connectivity, instead, apps installed in these watches run APIs to request data from the Zepp app. Additionally, these apps may also have a standalone web page to manage its settings, for example, a Weather application on the watch may need setting a location first from its settings page. These setting pages are a single bundled JavaScript file, so when they are run in the Zepp app's webview, the JavaScript code can send data to webview, and thus it can exchange data with the connected watch to the phone.

But since Zepplin is a re-implementation that doesn't depend on Zepp app, Zepplin will act as like a Zepp app in this scenario, but instead exchanging data with a real device, [`emulator`](../emulator/) will be used, thus the emulator will start along with the webview once it is launched. This makes it possible to share messages between JavaScript (running in webview) and Zepp OS (running in QEMU).
