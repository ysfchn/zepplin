#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from pathlib import Path
from typing import IO, Any, Dict, Optional, Type
from io import BytesIO
from importlib.resources import files
from tempfile import NamedTemporaryFile
from zipfile import ZipFile

from zepplin.constants import Language
from zepplin.emulator.classes import Emulator
from zepplin.logging import get_logger
from zepplin.webview.ble import (
    BluetoothMessageData, 
    BluetoothMessageFlag, 
    BluetoothMessageType, 
    BluetoothMessageVersion, 
    parse_message, 
    dump_message
)

import orjson
import webview
from httpx import Client

class Webview:
    """
    Run Side Service and Settings UI APIs for Zepp OS mini-apps.

    Some mini-apps contains a side-service to access more capabilities that the watch itself can't provide,
    such as connectivity, so these mini-apps can send commands to the Zepp app (Zepp's mobile app), and the
    Zepp app can send back the data to the app. For example, a mini-app may want to send a HTTP request
    through the phone, so it can display it on the watch.

    See Zepp's documentation for more about the architecture of mini-apps:
    https://docs.zepp.com/docs/guides/architecture/arc/
    
    But since Zepplin is a re-implementation that doesn't depend on Zepp app, our computer will act as like a 
    Zepp app in this scenario.

    This class will launch a webview to load Zepplin's JavaScript files and the specified mini-app's 
    background/settings scripts. Since mini-apps may want to send arbitrary messages between itself and 
    Zepp app in real-time, this Webview will also launch the Emulator, so we can share messages between 
    JavaScript (in webview) and Zepp OS (in QEMU emulator).
    """

    __slots__ = (
        "_user_data",
        "_side_script",
        "_settings_script",
        "_app_bundle",
        "_app_id",
        "_app_state",
        "_window",
        "_emulator",
        "_messaging_port1",
        "_messaging_port2",
        "_language",
        "_logger",
        "_http",
        "_webview_app_logger",
        "_webview_runner_logger"
    )

    def __init__(
        self,
        app_state : IO[bytes],
        qemu_bin : Path,
        kernel_file : Path,
        storage_file : Path,
        app_bundle : IO[bytes],
        app_id : int,
        side_script : Optional[IO[bytes]] = None,
        settings_script : Optional[IO[bytes]] = None,
        language : Language = Language.EN_US
    ):
        self._app_state = app_state
        self._app_bundle = app_bundle
        self._app_id = app_id
        self._side_script = side_script
        self._settings_script = settings_script
        self._window : Optional[webview.Window] = None
        self._messaging_port1 : int = 20
        self._messaging_port2 : int = 0
        emulator_class = type("EmulatorWithWebview", (Emulator, ), {
            "_bluetooth_received": lambda _, data: self._got_bluetooth_data(data)
        })
        self._emulator : Emulator = emulator_class(qemu_bin, kernel_file, storage_file)
        self._language = language
        # I guess there are no available values other than "lang"
        self._user_data = {
            "lang": self._language.value
        }
        self._logger = get_logger("webview")
        self._http = Client()
        self._webview_app_logger = self._logger.getChild("app")
        self._webview_runner_logger = self._logger.getChild("runner")

    def start(self):
        """
        Start both the webview and emulator, and block until webview exits.
        """
        self._window = webview.create_window(
            "zepplin-webview", 
            url = "about:blank", 
            js_api = self._get_js_bridge()
        )
        webview.start(self._init_window, gui = "qt", debug = True)

    # We write the application user data as in JSON lines format,
    # (every line is a independent JSON object), this eliminates the 
    # problem of writing malformed JSON that occur when overwriting
    # data to the same file multiple times at the same time, since
    # we don't seek to the start of the file.
    # TODO: Or maybe create a write/commit cache?

    def get_app_data(self) -> Dict[str, Any]:
        self._app_state.seek(0)
        data = self._app_state.read().split(b"\n")
        if (len(data) < 2) or (data[-2] == b"\n"):
            data = b"{}"
        else:
            data = data[-2].strip()
        self._app_state.flush()
        return orjson.loads(data)

    def save_app_data(self, data : Dict[str, Any]):
        self._app_state.write(orjson.dumps(data) + b"\n")
        self._app_state.flush()

    @classmethod
    def load_package(
        cls : Type["Webview"], 
        app_package : Path, 
        app_state : Path,
        qemu_bin : Path,
        kernel_file : Path,
        storage_file : Path
    ):
        """
        Creates a Webview object with side service and settings script extracted from 
        given mini-app package.
        """
        side_script = NamedTemporaryFile("w+b")
        settings_script = NamedTemporaryFile("w+b")
        app_code = BytesIO()
        app_id = None
        with ZipFile(app_package, "r") as zipf:
            if "app-side.zip" not in zipf.namelist():
                raise ValueError("This package doesn't contain a settings page or side app.")
            with zipf.open("app-side.zip", "r") as inner:
                with ZipFile(inner, "r") as app_side:
                    entries = app_side.namelist()
                    if "setting.js" in entries:
                        with app_side.open("setting.js") as file1:
                            settings_script.write(file1.read())
                            settings_script.flush()
                    if "app-side.js" in entries:
                        with app_side.open("app-side.js") as file2:
                            side_script.write(file2.read())
                            side_script.flush()
                    with app_side.open("app.json") as file3:
                        app_id = orjson.loads(file3.read())["app"]["appId"]
            app_code.write(zipf.read("device.zip"))
        app_code.seek(0)
        if not app_state.exists():
            app_state.touch()
        return cls(
            app_state = app_state.open("r+b"),
            app_bundle = app_code,
            app_id = app_id,
            side_script = side_script,
            settings_script = settings_script,
            qemu_bin = qemu_bin,
            kernel_file = kernel_file,
            storage_file = storage_file
        )

    def _receive_js_command(self, name : str, data : Dict[str, Any]):
        """
        Perform tasks based on the action name that called from JavaScript.
        """
        match name:
            case "init":
                self._emulator.wait_for_commands()
                return {
                    "status": True
                }
            case "load_data":
                return {
                    "user": self._user_data,
                    "languages": Language.dump_dict(),
                    "app": self.get_app_data()
                }
            case "save_data":
                self.save_app_data(data["app"])
            case "get_script":
                result : Dict[str, Optional[str]] = { "settings": None, "side": None }
                if self._settings_script:
                    self._settings_script.seek(0)
                    result["settings"] = self._settings_script.read().decode("utf-8")
                if self._side_script:
                    self._side_script.seek(0)
                    result["side"] = self._side_script.read().decode("utf-8")
                return result
            case "send_bluetooth":
                self._send_bluetooth_to_emulator(dump_message(BluetoothMessageData(
                    flag = BluetoothMessageFlag.APP,
                    version = BluetoothMessageVersion.VERSION_1,
                    type = BluetoothMessageType.DATA,
                    port1 = self._messaging_port1,
                    port2 = self._messaging_port2,
                    app_id = self._app_id,
                    extra = 0,
                    payload = bytes.fromhex(data["data"])
                )))
            case "send_log":
                levelName = data["level"]
                level = \
                    10 if levelName == "debug" else \
                    20 if levelName == "info" else \
                    30 if levelName == "warn" else \
                    40 if levelName == "error" else 10
                self._webview_app_logger.log(level, data["message"])
            case "send_http":
                resp = self._http.request(
                    method = data["method"],
                    url = data["url"],
                    content = data.get("body", None),
                    headers = data.get("headers", None),
                    # fetch() follows requests by default.
                    follow_redirects = True
                )
                self._webview_runner_logger.log(10, 
                    f"HTTP response: status={resp.status_code}, url={resp.url}"
                )
                return {
                    "url": str(resp.url),
                    "status": resp.status_code,
                    "redirected": len(resp.history) > 1,
                    "headers": dict(resp.headers),
                    "content": resp.read().decode("utf-8")
                }

    def _get_js_bridge(self):
        """
        Create a class that its methods will be exposed to JavaScript side for pywebview.
        https://pywebview.flowrl.com/guide/interdomain.html#run-python-from-javascript
        """
        def get_partial_method(name : str):
            def params(_, data : dict):
                return self._receive_js_command(name, data)
            return params
        return type("JSBridge", (), {
            "load_data": get_partial_method("load_data"),
            "save_data": get_partial_method("save_data"),
            "get_script": get_partial_method("get_script"),
            "send_bluetooth": get_partial_method("send_bluetooth"),
            "send_log": get_partial_method("send_log"),
            "send_http": get_partial_method("send_http"),
            "init": get_partial_method("init")
        })()

    def _get_html_asset(self):
        """
        Returns the index.html file contents inside "assets" package.
        """
        package = ".".join(__name__.split(".")[:-1])
        for file in files(package).joinpath("assets").iterdir():
            if file.name == "index.html":
                with file.open("r", encoding = "utf-8") as f:
                    return f.read()
        raise ValueError(
            "Can't locate the index.html file in the 'assets' package, "
            "make sure that it is placed in the right place."
        )

    def _send_bluetooth_to_emulator(self, data : bytes):
        """
        Send a data message to emulator.
        """
        self._webview_runner_logger.log(10, 
            f"Sending Bluetooth bytes to emulator: {data.hex()}"
        )
        self._emulator._bluetooth_send(data)

    def _send_bluetooth_to_webview(self, data : bytes):
        """
        Send a data message to webview.
        """
        data_hex = data.hex()
        self._webview_runner_logger.log(10, 
            f"Sending Bluetooth bytes to webview: {data.hex()}"
        )
        self._window.evaluate_js( # type: ignore
            'globalThis.currentZepplin.events.emit("bluetooth", "{0}")'.format(data_hex)
        )

    def _got_bluetooth_data(self, data : bytes):
        """
        Send incoming Bluetooth message from emulator to webview.
        """
        self._webview_runner_logger.log(10, 
            f"Received Bluetooth bytes from emulator: {data.hex()}"
        )
        message = parse_message(data)
        # If message is SHAKE, reply back with another SHAKE message.
        if (message.flag == BluetoothMessageFlag.APP) and (message.type == BluetoothMessageType.SHAKE):
            self._messaging_port1 = message.port1
            self._messaging_port2 = message.port2
            self._send_bluetooth_to_emulator(dump_message(BluetoothMessageData(
                flag = BluetoothMessageFlag.APP,
                version = BluetoothMessageVersion.VERSION_1,
                type = BluetoothMessageType.SHAKE,
                port1 = message.port1,
                port2 = message.port2,
                app_id = message.app_id,
                extra = 0,
                payload = message.app_id.to_bytes(4, "little")
            )))
        # Pass-through DATA payloads
        elif (message.flag == BluetoothMessageFlag.APP) and (message.type == BluetoothMessageType.DATA):
            self._send_bluetooth_to_webview(message.payload)
        else:
            self._webview_runner_logger.log(40, 
                f"Unsupported Bluetooth payload: {message._asdict()}"
            )

    def _init_window(self):
        parent = "/".join(__name__.split(".")[:-1])
        self._window.load_html( # type: ignore
            content = self._get_html_asset(),
            base_uri = (Path.cwd() / parent).absolute().as_uri()
        )
        self._emulator.start(False)
        # Install the given app to the emulator automatically.
        self._emulator.wait_for_commands()
        self._emulator.command_install_app(self._app_bundle, self._app_id)
        self._emulator.command_open_app(self._app_id)