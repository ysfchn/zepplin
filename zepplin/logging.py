import logging

def setup_logging():
    logging.basicConfig(
        filename = "zepplin.log", 
        style = "{", 
        format = "{asctime} [{levelname:.1}] {name} | {message}",
        encoding = "utf-8", errors = "ignore",
        datefmt = "%H:%M:%S"
    )

def get_logger(name : str):
    logger = logging.getLogger(name)
    logger.setLevel(10)
    return logger