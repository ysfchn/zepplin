#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import re
from enum import Enum
from typing import Annotated, Any, Dict, List, Literal, NamedTuple, Optional, TypedDict, Union


# -------------------------------------
#  General
# -------------------------------------

# Analytics domains that Zepp CLI calls, but of course we won't 
# implement the analytic functions in Zepplin:
# prod: https://api-analytics.huami.com
# test: https://api-analytics-test.huami.com

# The results are returned in beta list is encrypted with a key.
# So this is the key in hexadecimal format that they are using.
# Cipher: XSalsa20-Poly1305
#
# /cli/private-modules/zeppos-app-utils/dist/config/device.js:decrypt()
DEVICES_LIST_BETA_DECRYPT_KEY = "1235511a7ce4d8ca2f3a4913f5ba56a9b474a1fa24cb7f78857ecb798f64112c"

# Update devices list in 2 hours (milliseconds)
# TODO: Unused since we don't need it for now.
DEVICES_LIST_REFRESH_TIME = 2 * 60 * 60 * 1000

# Zepplin doesn't require Zepp Simulator, but here's the link to downloads if needed:
# https://docs.zepp.com/docs/guides/tools/simulator/download/

# Which version of Zeus CLI is implemented in Zepplin.
# Only used for comparing minimum version against emulator images.
CLI_BASED_ON_VERSION = "1.5.22" # TODO: Unimplemented

# The same version of axios is used in Zepp CLI (zeus), we also
# set the same user agent just to be safe.
USER_AGENT_AXIOS     = "axios/0.27.2"
USER_AGENT_NODEFETCH = "node-fetch"

USER_AGENT_ZEPP      = "Zepp/8.5.3-play (2201116SG; Android 12; Density/2.525)"
USER_AGENT_BROWSER   = "Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0"

# -------------------------------------
#  Authorization & API
# -------------------------------------

# Authorization callback address; after successfully authenticating with a Zepp 
# account, it will redirect to this address, where our local HTTP server is served, 
# so we can get the credentials from query parameters.
LOGIN_HTTP_SERVER_ADDR = "127.0.0.1:7650"
LOGIN_HTTP_SERVER_CALLBACK_PATH = "/login/callback"

LOGIN_PROD_URL = "https://user.zepp.com/universalLogin/index.html#/login"
LOGIN_TEST_URL = "https://user-testing.zepp.com/universalLogin/index.html#/login"

LOGIN_XIAOMI_URL = "https://account.xiaomi.com/oauth2/authorize"
LOGIN_XIAOMI_CALLBACK_URL = "https://hm.xiaomi.com/watch.do"

REGISTER_PROD_URL = "https://user.zepp.com/universalLogin/index.html#/register"

PROJECT_CLI_NAME       = "zeppos_zeus_cli"
PROJECT_SIMULATOR_NAME = "zeppos_simulator" # TODO: Not in use
PROJECT_OPEN_PLATFORM  = "open_platform" # Shows third-party sign options, not used in the code. TODO: Not in use
PLATFORM_APP           = "com.huami.webapp"
PLATFORM_SIMULATOR     = "com.huami.zeppos.simulator"

# Login tokens expire in 7 days (as milliseconds), so we add 7 days to system time.
# When signed in with Xiaomi account, the time that returned by server will be 
# used instead (seems like 30 days).
AUTH_TOKEN_EXPIRE_TIME = 1000 * 3600 * 24 * 7

# https://upload-cdn.huami.com/tposts/4436118
# This page that appears when successfully signed with CLI, but it is not used
# in Zepplin, as it doesn't really serve a purpose, just a static page.

# TODO: Not in use:
# API_COUNTRY      = "https://{account_host}/recommendCountry"
# API_DEVICE_META  = "https://{api_host}/device/settings/meta?deviceSources=260,261"

# It is only used for retrieving foreign sessions, for all other cases,
# Zepplin relies on CNAMEs returned from authorization response already.
DEFAULT_CNAMES = [
    "account-us3.zepp.com",
    "api-user-us3.zepp.com",
    "api-mifit-us3.zepp.com",
    "auth-us3.zepp.com",
    "api-open-us3.zepp.com"
]

API_USER         = "https://{api_host}/users/{user_id}"
API_LOGOUT       = "https://{account_host}/v1/client/logout"
API_USER_CENTER  = "https://{auth_host}/v2/developer/account/center"
API_USER_ADMIN   = "https://{account_host}/admin/users/{user_id}/profile"
API_DEVICE_ROM   = "https://{api_host}/devices/ALL/hasNewVersion"
API_DEVICES      = "https://{api_host}/users/{user_id}/devices"
API_DEVICE_FILE  = "https://{api_host}/apps/com.huami.midong/fileTypes/{file}/files"
API_DEVICE_MASK  = "https://{api_host}/market/devices/{device_id}/setting"
API_BRIDGE_RELAY = "https://{api_host}/developerbridge/client/relay"
API_BRIDGE_LIMIT = "https://{api_host}/custom/user/test-group/{group_id}/is-in"

API_PACKAGE_UPLOAD   = "https://{api_host}/custom/tools/app-dial/upload" # TODO: Not in use
API_PACKAGE_DOWNLOAD = "https://{api_host}/custom/tools/app-dial/download/{code}" # TODO: Not implemented


API_MARKET_HOME          = "https://{api_host}/market/devices/{device_id}/{store_type}/homepage"
API_MARKET_ITEMS         = "https://{api_host}/market/devices/{device_id}/{store_type}/apps"
API_MARKET_ITEM_INFO     = "https://{api_host}/market/devices/{device_id}/{store_type}/apps/{item_id}"
API_MARKET_ITEM_VERSIONS = "https://{api_host}/market/devices/{device_id}/{store_type}/apps/{item_id}/history"
API_MARKET_DEMO          = "https://{api_host}/market/watch/apps/1"

# API_MARKET_FAVORITES = "https://{api_host}/market/devices/{device_id}/{store_type}/favorites?page=1&per_page=10&userid=&user_country=&user_region=&api_level=100&sn="
# API_MARKET_PURCHASED = "https://{api_host}/market/devices/{device_id}/{store_type}/purchased-apps?page=1&per_page=10&userid=&user_country=&user_region=&api_level=100&sn="
# API_MARKET_BUILTIN   = "https://{api_host}/market/devices/{device_id}/{store_type}/builtin?builtin_ids=166130003,166130004,166130001,166120001,166130002&userid=&user_country=&user_region=&api_level=100&sn="
# API_MARKET_NEW       = "https://{api_host}/market/devices/{device_id}/{store_type}/new?userid=&user_country=&user_region=&api_level=100&sn="

STORE_TYPE_WATCH = "watch"
STORE_TYPE_APP   = "lightapp"

API_HUAMI_LOGIN_TOKEN    = "https://account.huami.com/v2/client/login"
API_HUAMI_LOGIN_PASSWORD = "https://api-user.huami.com/registrations/{email}/tokens"

GROUP_ID_1_PROD, GROUP_ID_2_PROD = 1659411611, 1659411646,
GROUP_ID_1_TEST, GROUP_ID_2_TEST = 1659342513, 1659412607,

# List of Zepp OS equipped devices with metadata about their technical specifications.
URL_DEVICES_PROD = "https://upload-cdn.zepp.com/zeppos/devkit/zeus/devices.json"
URL_DEVICES_BETA = "https://upload-cdn.zepp.com/zeppos/devkit/zeus/devices.beta.json"
URL_DEVICES_TEST = "https://upload-testing-cdn.huami.com/zeppos/devkit/zeus/devices.json" # TODO: unused

# -------------------------------------
#  Emulator
# -------------------------------------

URL_EMULATOR_DOWNLOADS_PROD = "https://upload-cdn.huami.com/zeppos/simulator/download/emulatorList.json"
URL_EMULATOR_DOWNLOADS_TEST = "https://upload-testing-cdn.huami.com/zeppos/simulator/download/emulatorList.json"

# The emulator kernel starts a websocket server to let us communicate 
# with the emulator, but since the websocket server IP is not directly 
# accessible from host, we forward the connection to localhost with
# QEMU parameters.
#
# The websocket server's IP and port seems to be constant so we 
# just hardcode the server address here.
#
# [!] DON'T CHANGE THIS, ZEPP OS SPECIFICALLY ASSIGNS THIS IP AND PORT
EMULATOR_WS_ORIGIN_ADDR = "192.168.166.188:7833"
EMULATOR_WS_ORIGIN_MASK = "192.168.166.1/24"

# Forward the connection to our own localhost, so we can connect
# to the websocket hosted inside the guest (emulator) from the 
# host machine.
EMULATOR_WS_FORWARD_ADDR = "127.0.0.1:7833"

# Websocket URL to send/receive commands between emulator
# and host, so this allows us to install/remove/open apps 
# on emulator.
#
# Note that the path is all caps, so "/SIMULATOR", the emulator also 
# serves a HTTP server under the same address in root ("/") path. 
# (maybe for demo purposes?)
EMULATOR_WS_COMMANDS_URL = f"ws://{EMULATOR_WS_FORWARD_ADDR}/SIMULATOR"

# Websocket URL to send/receive commands between emulator 
# and mini-app, aka. hmBle (Bluetooth) module.
EMULATOR_WS_BLUETOOTH_URL = f"ws://{EMULATOR_WS_FORWARD_ADDR}"
EMULATOR_WS_BLUETOOTH_SUBPROTOCOL = "HMSDK"

# Websockets running inside the emulator does return "Success!" and
# "Error!" messages in byte frame, depending the status of the given
# command.
EMULATOR_WS_PAYLOAD_SUCCESS = b"Success!"
EMULATOR_WS_PAYLOAD_ERROR = b"Error!"

# These pages are already re-implemented in zepplin.webview package;
#
# APP_SETTINGS_PROD_URL = "http://zepp-os.zepp.com/app-settings/v1.0.0/index.html"
# APP_SETTINGS_TEST_URL = "http://zepp-os-staging.zepp.com/app-settings/v1.0.0/index.html"
# APP_SERVICE_PROD_URL = "http://zepp-os.zepp.com/frameworks/v3.0.2/side-service.html"
# APP_SERVICE_TEST_URL = "http://zepp-os-staging.zepp.com/frameworks/v1.0.1/side-service.html"

# -------------------------------------
#  Bridge
# -------------------------------------

BRIDGE_CLIENT_TYPE_DEBUG = "debug"
BRIDGE_CLIENT_TYPE_DEVEL = "development"
BRIDGE_CLIENT_TYPES = Literal["debug", "development"]

BRIDGE_CLIENT_SIMULATOR  = (BRIDGE_CLIENT_TYPE_DEBUG, "Simulator")
BRIDGE_CLIENT_CLI        = (BRIDGE_CLIENT_TYPE_DEVEL, "CLI")
BRIDGE_CLIENT_APP        = (BRIDGE_CLIENT_TYPE_DEBUG, "app-Android")

# -------------------------------------
#  Enums
# -------------------------------------

class DeviceType(Enum):
    VDEVICE = -1
    MILI = 0
    SENSORHUB = 2
    WEIGHT = 1
    SHOES = 3
    WATCH = 4
    OTHER = 5
    TREADMILL = 6
    HEADSET = 7
    BLOOD_PRESSURE = 8
    EARBUD = 9
    HEARING_AID = 12
    RING = 13

# TODO: In Zepplin, there are booleans like "is_watchface", means it is either an app or watchface,
# but since Workout Extensions are added, I guess it would be better to use this enum instead.
class AppType(Enum):
    APP = "app"
    """
    An app that shows in the application drawer.

    Starting from Zepp OS version 3.0 and up, application can register a service that will be run even
    after the app has closed. See: https://docs.zepp.com/docs/guides/framework/device/app-service/
    """

    WATCHFACE = "watchface"
    """
    Watchface is the type of app that launches along with the system, and stays awake to display the time,
    date, heartrate and other status information.
    """

    WORKOUT_EXTENSION = "workout_extension"
    """
    Workout extension is a type of app that is used to extend the features of the built-in Workout app,
    which acts like a "workout plugin" in the system.

    Workout Extensions are only available starting from Zepp OS version 3.5 and up. However, in another
    page, Zepp mentions that the app needs to target at least 3.6 (not 3.5) to develop Workout Extensions.

    https://docs.zepp.com/docs/guides/workout-extension/intro/
    """

# https://docs.zepp.com/docs/1.0/reference/related-resources/language-list/
class Language(Enum):
    ZH_CN = "zh-CN"	
    """Simplified Chinese"""
    ZH_TW = "zh-TW"	
    """Traditional Chinese (Taiwan, China)"""
    EN_US = "en-US"	
    """English (USA)"""
    ES_ES = "es-ES"	
    """Spanish (Spain)"""
    RU_RU = "ru-RU"	
    """Russian (Russia)"""
    KO_KR = "ko-KR"	
    """Korean (Korea)"""
    FR_FR = "fr-FR"	
    """French (France)"""
    DE_DE = "de-DE"	
    """German (Germany)"""
    ID_ID = "id-ID"	
    """Indonesian"""
    PL_PL = "pl-PL"	
    """Polish (Poland)"""
    IT_IT = "it-IT"	
    """Italian (Italy)"""
    JA_JP = "ja-JP"	
    """Japanese (Japan)"""
    TH_TH = "th-TH"	
    """Thai"""
    AR_EG = "ar-EG"	
    """Arabic (Egypt)"""
    VI_VN = "vi-VN"	
    """Vietnamese"""
    PT_PT = "pt-PT"	
    """Portuguese (Portugal)"""
    NL_NL = "nl-NL"	
    """Dutch"""
    TR_TR = "tr-TR"	
    """Turkish (Turkey)"""
    UK_UA = "uk-UA"	
    """Ukrainian"""
    IW_IL = "iw-IL"	
    """Hebrew (Israel)"""
    PT_BR = "pt-BR"	
    """Portuguese (Brazil)"""
    RO_RO = "ro-RO"	
    """Romanian"""
    CS_CZ = "cs-CZ"	
    """Czech"""
    EL_GR = "el-GR"	
    """Greek"""
    SR_RS = "sr-RS"	
    """Serbian (Latin)"""
    CA_ES = "ca-ES"	
    """Catalan"""
    FI_FI = "fi-FI"	
    """Finnish"""
    NB_NO = "nb-NO"	
    """Norwegian"""
    DA_DK = "da-DK"	
    """Danish"""
    SV_SE = "sv-SE"	
    """Swedish"""
    HU_HU = "hu-HU"	
    """Hungarian"""
    MS_MY = "ms-MY"	
    """Malay"""
    SK_SK = "sk-SK"	
    """Slovakian"""
    HI_IN = "hi-IN"	
    """Hindi"""

    def to_index(self):
        """
        Get the enum index of this Language.
        """
        return list(self.__class__.__members__.values()).index(self)

    @classmethod
    def from_index(cls, index : int):
        """
        Get a Language based on enum index.
        """
        return list(cls.__members__.values())[index]

    @classmethod
    def dump_dict(cls) -> Dict[str, Union[str, int]]:
        """
        Create a dictionary with each language code mapped to their index,
        and each index mapped to their language code. Used for passing the full
        language table to JavaScript implementations of side service and settings 
        app pages.

        { "en-US": 2, 2: "en-US" ... }
        """
        output = {}
        for index, member in enumerate(cls.__members__.values()):
            output[str(index)] = member.value
            output[member.value] = index
        return output

class DeviceShape(Enum):
    SQUARE = "square"
    ROUND = "round"
    BAR = "bar"

class DeviceCapabilities(Enum):
    GPS = "gps"
    ACCELEROMETER = "accelerometer"
    MAGNETOMETER = "magnetometer"
    GRYOSCOPE = "gyroscope"
    ALTIMETER = "altimeter"

class SystemVersion(Enum):
    OS_1 = "1.0"
    OS_2 = "2.0"
    OS_2_1 = "2.1"
    OS_3 = "3.0"
    OS_3_5 = "3.5" # Only used as a range
    OS_3_6 = "3.6" # Only used as a range
    OS_3_7 = "3.7" # Only used as a range

# -------------------------------------
#  Logs
# -------------------------------------

# https://stackoverflow.com/a/3143231
PATTERN_ISO_DATETIME = br"\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d{3}(?:[+-][0-2]\d:[0-5]\d|Z)"
PATTERN_ANSI = br"(?:\x1B[@-Z\\-_]|[\x80-\x9A\x9C-\x9F]|(?:\x1B\[|\x9B)[0-?]*[ -/]*[@-~])"

PATTERN_LOG_SEVERITY_CODE = br'(?:[IDWE])'
PATTERN_LOG_SEVERITY_NAME = br'(?:log|debug|info|warn|error)'

# Should match:
# [2024-05-13T02:19:44.178Z] [jsfwk.info ] [framework] [-] framework version => 3.0
# [prefix] [2024-05-13T02:19:44.178Z] [jsfwk.info ] [framework] [-] framework version => 3.0
PATTERN_LOG_JSFWK = (
    br"(?:\[(?P<j_prefix>.*?)\] )?"
    br"\[(?P<j_datetime>" + PATTERN_ISO_DATETIME + br")\] "
    br"\[jsfwk\.(?P<j_severity>" + PATTERN_LOG_SEVERITY_NAME + br") *\] "
    br"\[(?P<j_name>.*?)\] "
    br"\[(?P<j_tag>.*?)\] "
)

# Should match:
# 5/12 [0]19:12:15.134 E/TM_MANAGER sys_init: get time zone failed, ret: 2 SYSPROP_ERROR_DEFAULTVALUE
PATTERN_LOG_ROOT = (
    br"^"
    br"(?:\$ )?\d+/\d+ \[0\]"
    br"(?P<time>\d{2}:\d{2}:\d{2}\.\d{3}) "
    br"(?P<severity>" + PATTERN_LOG_SEVERITY_CODE + br")/"
    br"(?P<source>[^ ]*) "
    br"(?:(?P<module>[^:]+): ?)?"
    br"(?:" + PATTERN_LOG_JSFWK + br")?(?P<message>.*)"
    br"$"
)

REGEX_LOG_CAPTURE = re.compile(PATTERN_LOG_ROOT)
REGEX_ANSI_ESCAPE = re.compile(PATTERN_ANSI)

# -------------------------------------
#  Misc
# -------------------------------------

# Port number that is used for all local web servers specific to Zepplin.
# It is not related to Zepp OS or/and emulator, so it can be safely changed.
# Why I chose 9377, because it is "ZEPP" in T9 :)
ZEPPLIN_WEB_SERVER_PORT = 9377

class _VarInfo(NamedTuple):
    default : Union[bool, str, int, Enum]
    ge : Optional[int] = None
    le : Optional[int] = None
    enum : Optional[List[Any]] = None

# Zepp OS weather conditions
# Taken from: https://docs.zepp.com/docs/watchface/specification/#type-data
class Weather(Enum):
    CLOUDY = 0
    SHOWER = 1
    SNOW_SHOWER = 2
    SUNNY = 3
    OVERCAST = 4
    LIGHT_RAIN = 5
    LIGHT_SNOW = 6
    MODERATE_RAIN = 7
    MODERATE_SNOW = 8
    HEAVY_SNOW = 9
    HEAVY_RAIN = 10
    SANDSTORM = 11
    SLEET = 12
    FOG = 13
    HAZE = 14
    THUNDERSHOWER = 15
    SNOWSTORM = 16
    DUST = 17
    EXTRAORDINARY_RAINSTORM = 18
    RAIN_WITH_HAIL = 19
    THUNDERSHOWERS_WITH_HAIL = 20
    HEAVY_RAINSTORM = 21
    SAND_BLOW = 22
    STRONG_SANDSTORM = 23
    RAINSTORM = 24
    UNKNOWN = 25
    CLOUDY_AT_NIGHT = 26
    SHOWER_AT_NIGHT = 27
    CLEAR_NIGHT = 28

# ./build/electron/constant/vconfig.js
# TODO: Use Weather enum for "weather" and Language for "language". 
# But I'm not sure if emulator supports all of these values.
class EmulatorSystemStatuses(TypedDict, total = False):
    calorie: Annotated[str, _VarInfo(300, ge = 0)]
    """Calories (>=0, default 300)"""
    heartrate: Annotated[int, _VarInfo(75, ge = 0, le = 255)]
    """Heartrate (0-255, default 75)"""
    walkstep: Annotated[int, _VarInfo(300, ge = 0)]
    """Number of steps (>=0, default 300)"""
    battery: Annotated[int, _VarInfo(88, ge = 0, le = 100)]
    """Battery level (0-100, default 88)"""
    distance: Annotated[int, _VarInfo(885, ge = 0)]
    """Distance (>=0, default 885)"""
    pai: Annotated[int, _VarInfo(75, ge = 0, le = 100)]
    """PAI (>=0, default 75)"""
    timeformat: Annotated[bool, _VarInfo(True)]
    """24-hour system (bool, default True)"""
    language: Annotated[int, _VarInfo(2, enum = [0, 1, 2])]
    """Language (in index, 0 is ZH_CN, 1 is ZH_TW and 2 is EN_US, default is 2)"""
    charge: Annotated[bool, _VarInfo(True)]
    """Battery charging (bool, default True)"""
    btcon: Annotated[bool, _VarInfo(True)]
    """Bluetooth connected (bool, default True)"""
    mute: Annotated[bool, _VarInfo(False)]
    """Mute (bool, default False)"""
    lock: Annotated[bool, _VarInfo(False)]
    """Lock (bool, default False)"""
    alarm: Annotated[bool, _VarInfo(True)]
    """Alarm (bool, default True)"""
    weather: Annotated[Weather, _VarInfo(Weather.CLOUDY, enum = [
        Weather.CLOUDY, Weather.SHOWER, Weather.SNOW_SHOWER,
        Weather.SUNNY, Weather.OVERCAST, Weather.LIGHT_RAIN,
        Weather.LIGHT_SNOW
    ])]
    """Weather condition (from `Weather` enum)"""
    temp: Annotated[int, _VarInfo(25, ge = -50, le = 50)]
    """Current temperature, in Celsius (low -50, max 50, default 25)"""
    aqi: Annotated[int, _VarInfo(133, ge = 0, le = 300)]
    """AQI (0-300, default 133)"""
    tempmax: Annotated[int, _VarInfo(29, ge = -50, le = 50)]
    """Maximum temperature, in Celsius (low -50, max 50, default 29)"""
    tempmin: Annotated[int, _VarInfo(22, ge = -50, le = 50)]
    """Minimum temperature, in Celsius (low -50, max 50, default 22)"""
    f_temphl: Annotated[bool, _VarInfo(True)]
    """High and low temperature marker (bool, default True)"""
    f_weather: Annotated[bool, _VarInfo(True)]
    """Weather forecast marker (bool, default True)"""
    f_tempnow: Annotated[bool, _VarInfo(True)]
    """Current temperature marker (bool, default True)"""
    f_aqi: Annotated[bool, _VarInfo(True)]
    """Current AQI marker (bool, default True)"""
    uvi: Annotated[int, _VarInfo(4, ge = 0, le = 5)]
    """UVI (0-5, default 4)"""
    humidity: Annotated[int, _VarInfo(50, ge = 0)]
    """Humidity level (>=0, default 50)"""
    sunrise_h: Annotated[int, _VarInfo(5, ge = 0)]
    """Sunrise hours (>=0, default 5)"""
    sunrise_m: Annotated[int, _VarInfo(50, ge = 0)]
    """Sunsire minutes (>=0, default 50)"""
    sunset_h: Annotated[int, _VarInfo(17, ge = 0)]
    """Sunset hours (>=0, default 17)"""
    sunset_m: Annotated[int, _VarInfo(50, ge = 0)]
    """Sunset minutes (>=0, default 50)"""
    window_level: Annotated[int, _VarInfo(4, ge = 0, le = 12)]
    """Wind power (0-12, default 4)"""
    window_dir: Annotated[int, _VarInfo(4, ge = 0, le = 7)]
    """Wind direction (0-7, default 4)"""
    moon_phase: Annotated[int, _VarInfo(4, ge = 0, le = 30)]
    """Moon phase (0-30, default 4)"""
    alarm_h: Annotated[int, _VarInfo(8, ge = 0, le = 24)]
    """Alarm hours (0-24, default 8)"""
    alarm_m: Annotated[int, _VarInfo(30, ge = 0, le = 60)]
    """Alarm minutes (0-30, default 30)"""
    goal_calorie: Annotated[int, _VarInfo(200, ge = 0)]
    """Calories goal (>=0, default 200)"""
    goal_walkstep: Annotated[int, _VarInfo(3000, ge = 0)]
    """Walkstep goal (>=0, default 3000)"""
    stand: Annotated[int, _VarInfo(2, ge = 0)]
    """Standing time (>=0, default 2)"""
    goal_stand: Annotated[int, _VarInfo(4, ge = 0)]
    """Standing time goal (>=0, default 4)"""
    sleep_h: Annotated[int, _VarInfo(7, ge = 0, le = 24)]
    """Sleep time hours (0-24, default 7)"""
    sleep_m: Annotated[int, _VarInfo(30, ge = 0, le = 60)]
    """Sleep time minutes (0-60, default 30)"""
    goal_sleep: Annotated[int, _VarInfo(6, ge = 0, le = 24)]
    """Sleep time hours goal (0-24, default 6)"""
    floor: Annotated[int, _VarInfo(10, ge = 0)] # TODO: What does this do?
    """Climbing (???) (>=0, default 10)"""
    stress: Annotated[int, _VarInfo(10, ge = 0)]
    """Stress (>=0, default 10)"""
    fat_burn: Annotated[int, _VarInfo(10, ge = 0, le = 100)]
    """Burned fat (0-100, default 10)"""
    goal_fat_burn: Annotated[int, _VarInfo(30, ge = 0, le = 100)]
    """Burned fat goal (0-100, default 30)"""
    spo2: Annotated[int, _VarInfo(95, ge = 0)]
    """SpO2 level (>=0, default 95)"""
    baro: Annotated[int, _VarInfo(60, ge = 0)]
    """Air pressure (barometer) (>=0, default 60)"""
    training_load: Annotated[int, _VarInfo(60, ge = 0)]
    """Exercise load (>=0, default 60)"""


# TODO: unimplemented
PERMISSION_LIST = (
    "access_activity",
    "access_aod",
    "access_app_cluster_storage",
    "access_calendars",
    "access_exercise",
    "access_heart_rate",
    "access_internet",
    "access_location",
    "run_background",
    "access_sleep",
    "access_user_profile"
)