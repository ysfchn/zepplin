#
#    Copyright (C) 2024 ysfchn
#
#    This file is part of Zepplin.
#
#    Zepplin is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Zepplin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from typing import IO, Dict, List, Tuple, Union
from hashlib import sha1

def get_emulator_uuid(device_id : str):
    return sha1(
        device_id.encode("ascii"), usedforsecurity = False
    ).hexdigest()[:8]

def make_qemu_args(
    args : Dict[str, Union[None, str, List[Union[str, Tuple[str, str]]]]]
):
    output = []
    for k, v in args.items():
        current_key = k
        if len(k) > 3:
            array = k[-3:]
            if array.startswith("[") and array.endswith("]"):
                current_key = k[:-3]
        output.append("-" + current_key)
        if v is not None:
            if isinstance(v, str):
                output.append(v)
            else:
                params = []
                for val in v:
                    if isinstance(val, str):
                        params.append(val)
                    else:
                        params.append(f"{val[0]}={val[1]}")
                output.append(",".join(params))
    return output


def chunked_read(
    buffer : IO[bytes], 
    chunk_size : int = 1024,
    max_size : int = -1
):
    total_read = 0
    while True:
        chunks = chunk_size
        if max_size > -1:
            chunks = min(chunk_size, max_size - total_read)
        data = buffer.read(chunks)
        total_read += chunks
        if data:
            yield data
            if total_read == max_size:
                break
        else:
            break


def app_id_to_hex(
    app_id : int
) -> str:
    return hex(app_id)[2:].upper().rjust(8, "0")