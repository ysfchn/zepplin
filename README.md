<div align="center">
    <img src="assets/zepplin.png" height="150">
    <h1>zepplin</h1>
    <p>One-stop tool for Zepp OS.</p>
</div>

---

Zepplin is a free (as-in freedom) implementation of [Zepp OS](https://docs.zepp.com/docs/intro/) development tools, thus serving as an alternative to [Zepp Simulator](https://docs.zepp.com/docs/1.0/guides/tools/simulator/) and [Zepp CLI](https://docs.zepp.com/docs/1.0/guides/tools/cli/).

> Zepplin is still under development. While most of the features are already available, they still may be refactored or deprecated over the time until the stable API has been created.

Zepplin is splitted to different packages that focuses on different aspects of Zepp OS development:

* [**Account**](./zepplin/api/) ─ Login with Zepp accounts, get user information, connect to Zepp Developer Bridge and so on
* [**Emulator**](./zepplin/emulator/) ─ Download and emulate Zepp OS firmware images on QEMU, so you can run mini-apps and test watchfaces
* [**Webview**](./zepplin/webview/) ─ JavaScript code to provide Side Service and Settings UI APIs

_The name "zepplin" is a pun on the words "zeppelin" (an airship) and "zepp". :P_

## Motivation

[Zepp OS](https://docs.zepp.com/docs/intro/) is a proprietary operating system (based on FreeRTOS) created by Zepp Health made for low-end affordable wearable devices. Devices that run Zepp OS are released & sold under Amazfit brand. Xiaomi also made an exception for once to put Zepp OS on their Smart Band 7 model (which I currently have) but that's an another story.

Thankfully, Zepp Health lets and encourages developers to build apps (or called as "mini-apps" by them) for wearable devices that run Zepp OS, so that means that I can get the most of my smart band even though it is sold for just a few bucks, so knowing that made me happy.

However, wish I could say the same for the developer experience, because after trying to make a simple app for my smart band, I noticed a lot of issues, for example their documentation just explains APIs with a few of words and nothing more about how/when it does work, which is a bummer. Also, since not every Zepp OS powered device receives the latest released Zepp OS version always, it causes a lot of breaking changes in the API, and that not only makes the development harder, it also makes the distribution painful. And to inspect the bugs/logs, (because the smart band don't give feedback on the coding errors other than a bugged yellow screen) it is required to use Zepp's own mobile app, which it is not possible for me because I'm using Xiaomi's product, not Amazfit's, so they don't acknowledge Xiaomi's only Zepp OS device due to their [arrangements with Xiaomi.](https://github.com/zepp-health/zeppos-docs/issues/16#issuecomment-1829190237) (even Xiaomi hides or doesn't share the fact that it runs Zepp OS.)

Even if I could own an Amazfit device to pair with Zepp app, I don't want to use the app anyway, because I'm already using Gadgetbridge to manage my smart band, so I can get the full control of my data, without relying on proprietary apps that connect to the cloud.

So I wanted to see if I can re-make the Zepp OS tools, to make developing the apps easier, uncovering and documenting every detail that I could find about Zepp OS, not having to depend on Zepp mobile app and making it fully free & open source, so everyone can benefit from it. And here it is.

Eventually and hopefully, Zepplin will become a one-stop place to build, package, run and debug Zepp OS mini-apps.

> Devices equipped with Zepp OS are usually more affordable and easier to buy for most underdeveloped countries, however, if you are looking for a privacy & free (as-in freedom) focused watch, Zepp OS is not of them, so you can check out Bangle.js or AsteroidOS if you are interested.

## Coverage

Implemented functions are listed below:

#### Account & Authorization

Signing with a Zepp account is not mandatory to emulate Zepp OS apps but an account is required for connecting Zepp services, such as to query users details and communicating with "Developer Bridge", explained below.

* [x] Login on browser
    * [x] Zepp accounts (directly created on Zepp, aka. Huami)
    * [x] Xiaomi accounts (linking with Zepp)
* [x] Login with access token
* [x] Login with e-mail and password (only for Zepp accounts)
* [x] Save credentials to file & import from saved credentials
* [x] Retrieve user information (username, display name, create time)
* [x] Retrieve user paired devices (MAC address, auth key, firmware/hardware version...)

#### Emulator

Run & debug Zepp OS apps with QEMU. Since Zepp OS kernel images don't work out-of-box with QEMU, Zepp uses a [custom build](https://github.com/zepp-health/qemu/), so their changes are dumped to a patch file and applied on top of clean QEMU source to build a trustworthy binary - instead of using Zepp's pre-compiled QEMU binaries.

* [ ] Set up CI to compile QEMU from source with patches
* [x] Download emulator firmware (kernel & storage) files
* [x] Launch QEMU with required arguments
* [x] Set up network to make emulator connection possible<sup>1</sup>
* [x] Send commands to emulator over websocket to:
    * [x] Install and remove apps
    * [x] Open apps
    * [x] Change system statuses (battery, step, weather...)
    * [x] Take screenshot (for Zepp OS 2.0 and up)
* [x] Developer bridge support (connection between Zepp accounts and development clients, see below)

<sup>1</sup> Unlike Zepp Simulator, Zepplin calls QEMU to not use a TUN/TAP network (which requires additional setup on host machine) but instead simply forward the network.

#### Development Bridge

Connect development tools to [a Zepp websocket endpoint](https://docs.zepp.com/docs/guides/faq/developer-bridge-mode/), so clients can see each other and send commands between them remotely, for example Zepp Watchface Maker website can send the built watchface app to websocket, and Zepplin can receive the command from websocket and make emulator to install this app.

* [x] Receive commands and call emulator
    * [x] Download and install apps
    * [x] Remove apps
    * [x] Send & receive logs
    * [ ] Take screenshot (implemented, but doesn't save the screenshot anywhere for now)

#### Mini-apps

Create, package and build Zepp OS applications.

* [ ] Build
    * [ ] ...
    * [x] Get list of Zepp OS devices
* [ ] Package
    * [ ] ...
* [ ] Distribution
    * [ ] Publish and generate a QR code for download
    * [ ] Send an intent to Gadgetbridge to flash the mini-app to device?
* [ ] Webview
    * [x] Launch emulator along with a webview
    * [x] Load side-app and settings page from given ZPK package
    * [x] Proxy `fetch()`
    * [x] Send commands between emulator and webpage (hmBle module) - needs more testing
    * [x] Implement modules (hmUtils, hmSetting, HmLogger...)
    * [ ] Zepp OS v3 modules (will be added after v1 support), file transfers, image convert...
    * [x] Settings page
        * [x] `AppSettingsPage`
        * [x] Render components
        * [x] Rebuild UI on settings change
    * [x] Side-app page
        * [x] `AppSideService`

## License

Zepplin is licensed under GNU AGPL v3 (or later).

## Disclaimer & Acknowledgements

Zepplin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

Zepplin is a clean-room implementation of Zepp OS tools; it doesn't directly depend, use, incorporate, inherit from any proprietary source code owned by Zepp Health. Some of Zepp OS tools such as the CLI are officially [exists on the NPM](https://www.npmjs.com/package/@zeppos/zeus-cli), and tarballs are already public, so there were no sophisticated attempts to examine it.

Zepplin intends to publicly document all protocols and schemas about Zepp OS and its tools as much as possible, so it can be useful for people who would like to work with Zepp OS, thus while if it is not directly intended, it may naturally cause exposing some details which are intended by Zepp Health to be not known to public. Since Zepplin is an alternative for official Zepp OS tools, some parts of it may require communicating with Zepp Health servers, however in any case, it doesn't attempt to use Zepp services or/and endpoints in a malicious intent.

This project is not supported, sponsored, affiliated, approved, or endorsed in any way with Zepp Health. "Zepp OS" is an product of Zepp Health.

[zmake](https://github.com/melianmiko/zmake) is a project that allows packaging Zepp OS apps, which influenced me to make Zepplin. And in fact, Zepplin adopts some code from zmake (which is licensed under GPLv3), especially for the generation of TGA images.